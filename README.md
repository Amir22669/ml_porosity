# ML_Porosity

### Estimation of Porosity in a Hydrocarbon Reservoir using Well Data by Multipermutation Hybrid Machine Learning

![Porosity Estimation](ml_poro.png)

This project aims to estimate porosity in a hydrocarbon reservoir using well data and various machine learning models. 

Four models have been implemented for this purpose: Convolutional Neural Network (CNN), Support Vector Regression (SVR), Fuzzy Logic, and a Hybrid Model that combines the outputs of the other three models.

The code is organized as follows:

1. `cnn`: This file contains the code for implementing a Convolutional Neural Network (CNN) model for porosity estimation. 
The file includes functions for building and training the model, as well as for making predictions on new well data.

2. `svr`: This file contains the code for implementing a Support Vector Regression (SVR) model for porosity estimation. 
The file includes functions for building and training the model, as well as for making predictions on new well data.

3. `fuzzy`: This file contains the code for implementing a Fuzzy model for porosity estimation. The file includes functions for building and training the model, as well as for making predictions on new well data.

4. `hybrid`: This file contains the code for implementing a Hybrid model for porosity estimation, which combines the CNN, SVR, and Fuzzy models. 
The file includes functions for building and training the hybrid model, as well as for making predictions on new well data.




Input data consists of well log data and core data. The input data is preprocessed to train a machine learning model.





## How to Use This Code

To use the code, simply run the respective script file for the desired model, passing in the relevant parameters and well data.

### Requirements

- MATLAB 2020 or later
- Input data: DataFile.mat (provided)
- Model: master  (provided)

### Instructions

1. Clone the repository to your local machine: `git clone https://github.com/Amir22669/ml_porosity.git`
2. Navigate to the project directory: `cd ml_porosity`
3. Copy the pre-prepared input data file (DataFile.mat) into the project directory
4. Run the quick-test file (CNN_model.m) by double-clicking on the file or running `matlab -r CNN_model.m` in the command line
5. The script will load the pre-trained CNN model and input data from DataFile.mat, and output the predicted porosity values


![Porosity Example](CNN_model.jpg)

### Note

The `master` branch contains the pre-trained CNN model along with the quick-test file. Additional branches may be available for different models or variations of the approach.



## Acknowledgments

This project was developed by Amirreza Mehrabi as part of SRBU's PhD research efforts. 



## License

This project is licensed under the MIT License - see the LICENSE file for details.
