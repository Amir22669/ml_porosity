function G = HyperbolicTangent(U,V)
    % Range(-1,1)
    k = 1 ;
    c = 1 ;
    G = tanh(k*U*V' + c) ;

end