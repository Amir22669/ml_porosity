function G = RBF(U,V)

    s = 1 ; gamma = 1/(2*(s^2)) ; G = exp(-(gamma)*(sum(U.*U ,2) -2* (U*V') + sum(V.*V,2))) ; 

end