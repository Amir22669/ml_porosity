function G = Gaussian(U,V)

    s = 1 ; G = exp(-(sum(U.*U ,2) -2* (U*V') + sum(V.*V,2))/(2*(s^2))) ;

end