function G = LaplaceRBF(U,V)

    s = 1 ; G = exp(-sqrt(abs(sum(U.*U ,2) -2* (U*V') + sum(V.*V,2)))/s) ; % gaussian (|U-V|^2 = U^2 -2UV + V^2)

end