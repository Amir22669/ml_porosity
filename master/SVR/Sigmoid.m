function G = Sigmoid(U,V)
    % Sigmoid kernel function with slope gamma and intercept c
    % Range(0,1)
    a = 1;
    b = -1;
    G = tanh(a*U*V' + b);
end