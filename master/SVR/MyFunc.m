function G = MyFunc(U,V)
% U(xj) : Num of Data(Row), Num of Feature (Column
% V(xk) : 1(Row) , Num of Feature (Column)
% G(xj,xk) : Num of Data(Row) , 1 (Column)

    G = (U*V') ; % linear
%     q = 3 ; G = (1+U*V').^q ; % poly
%     s = 1 ; G = exp(-(sum(U.*U ,2) -2* (U*V') + sum(V.*V,2))/(2*(s^2))) ; % gaussian (|U-V|^2 = U^2 -2UV + V^2)
%     s = 1 ; gamma = 1/(2*(s^2)) ; G = exp(-(gamma)*(sum(U.*U ,2) -2* (U*V') + sum(V.*V,2))) ; % rbf , gamma>0 (|U-V|^2 = U^2 -2UV + V^2)
    
end