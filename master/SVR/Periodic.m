function G = Periodic(U,V)
    l = size(U,1) ;
    s = 1 ;
    p = -1 ;
    X_Y = sqrt(abs(sum(U.*U ,2) -2* (U*V') + sum(V.*V,2))) ;
    G = (s^2)*exp(-(2*((sin(pi*(X_Y)/p)).^2))/(l.^2)) ; 
    
end