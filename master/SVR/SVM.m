clear all ; clc ; close all ; %% SVM %%
warning off ;
%%
load DataFile.mat
WellNames = DataStruct.WellNames ;
NumberOfHoles = DataStruct.NumberOfHoles ;
%%
Structural = {'linear' , 'gaussian' , 'polynomial' , 'rbf' , 'LaplaceRBF' , 'Periodic' , 'Sigmoid' , 'HyperbolicTangent'} ;
FigureFlag = false ;

%%
for i = 4%:length(WellNames)

    % from Core properties and porosity
    DataStruct.(WellNames{i}).Core_Properties.PHIE = DataStruct.(WellNames{i}).Core_Porosity ;
    try
        DataStruct.(WellNames{i}).Core_Properties.DT = [] ; % Remove DT
    catch
    end
    DataStruct.(WellNames{i}).Core_Properties = rmmissing(DataStruct.(WellNames{i}).Core_Properties) ; % Remove NaN Rows


    DT = DataStruct.(WellNames{i}).Core_Properties.DEPTH ;
    YT = DataStruct.(WellNames{i}).Core_Properties.PHIE ;
    XT = [DataStruct.(WellNames{i}).Core_Properties.BS DataStruct.(WellNames{i}).Core_Properties.CALI DataStruct.(WellNames{i}).Core_Properties.GR DataStruct.(WellNames{i}).Core_Properties.NPHI DataStruct.(WellNames{i}).Core_Properties.RHOB DataStruct.(WellNames{i}).Core_Properties.DRHO] ;

    Sec_Range = [min(DT) max(DT)] ;


    for k = 5% %length(Structural)
        Function = Structural{k} ;
        WantStruct = ['SVM' Structural{k}] ;
    
        % Train
        switch Function
            case 'polynomial'
                Degree = 4 ;
                Model = fitrsvm(XT,YT,'Standardize',true,'KernelFunction',Function,'PolynomialOrder',Degree) ; % For polynomial: 'PolynomialOrder',4
            otherwise
                Model = fitrsvm(XT,YT,'Standardize',true,'KernelFunction',Function) ;
        end
        
        % CrossValidation
        CV = crossval(Model,'Holdout',0.3) ;
        %
        indextest = find(~isnan(kfoldPredict(CV))) ; % cross-validation data
        indextrain = find(isnan(kfoldPredict(CV))) ; % non-cross-validation data
        
        XTest = XT(indextest,:) ;
        YTest = YT(indextest,:) ;
        DTest = DT(indextest,:) ;
        XTrain = XT(indextrain,:) ;
        YTrain = YT(indextrain,:) ;
        DTrain = DT(indextrain,:) ;
        net = CV.Trained{1} ;
        if FigureFlag
            figure('Color','w','Name',[WellNames{i} ' , ' WantStruct] , 'NumberTitle','off')
        end
        
    
        for j = 0:NumberOfHoles(i) 
            if FigureFlag
                Ax(j+1) = subplot(1,NumberOfHoles(i)+2,j+1) ; Ax(j+1).XColor = 'none' ; Ax(j+1).YColor = 'none' ;
                try
                    DataStruct.(WellNames{i}).(['Log_' num2str(j)]).DT = [] ; % Remove DT
                catch
                end
                DataStruct.(WellNames{i}).(['Log_' num2str(j)]) = rmmissing(DataStruct.(WellNames{i}).(['Log_' num2str(j)])) ;
                if ~isempty(DataStruct.(WellNames{i}).(['Log_' num2str(j)]))
                    Ax(j+1).XColor = 'black' ; Ax(j+1).YColor = 'black' ;
        
                    XL = [DataStruct.(WellNames{i}).(['Log_' num2str(j)]).BS DataStruct.(WellNames{i}).(['Log_' num2str(j)]).CALI DataStruct.(WellNames{i}).(['Log_' num2str(j)]).GR DataStruct.(WellNames{i}).(['Log_' num2str(j)]).NPHI DataStruct.(WellNames{i}).(['Log_' num2str(j)]).RHOB DataStruct.(WellNames{i}).(['Log_' num2str(j)]).DRHO] ;
                    YL = DataStruct.(WellNames{i}).(['Log_' num2str(j)]).PHIE ;
                    DL = DataStruct.(WellNames{i}).(['Log_' num2str(j)]).DEPTH ;
        
                    h1 = plot(Ax(j+1),YL,DL,'g','LineWidth',2) ; Ax(j+1).YDir = 'reverse' ; hold(Ax(j+1),'on') ;
                    h2 = plot(Ax(j+1),predict(net,XL),DL,'b','LineWidth',2) ;
                    h3 = plot(Ax(j+1),YT,DT,'Marker','o','MarkerFaceColor','g','MarkerSize',7,'MarkerEdgeColor','k','LineStyle','none') ;
                    h4 = scatter(Ax(j+1),predict(net,XTrain),DTrain,23,'Marker','o','MarkerFaceColor',[0.1 0.1 0.9],'MarkerEdgeColor',[0.9370 0.9680 0.0190],'MarkerFaceAlpha',0.8) ;
                    h5 = scatter(Ax(j+1),predict(net,XTest),DTest,23,'Marker','o','MarkerFaceColor',[0.9 0.1 0.1],'MarkerEdgeColor',[0.9290 0.6940 0.1250],'MarkerFaceAlpha',0.8) ;
        % 
                    Err = mape(YL,double(predict(net,XL)))*100 ;
                    Err(Err>100) = 100 ;
        % 
                    title(Ax(j+1),['Log Error_{MAPE}: ' num2str(Err,'%2.2f') '%'],'FontSize',7)
                    Ax(j+1).XAxis.Limits(2) = 0.4 ; Ax(j+1).XAxis.Limits(1) = 0 ;
                    Ax(j+1).YAxis.Limits(2) = Sec_Range(2)+1 ; Ax(j+1).YAxis.Limits(1) = Sec_Range(1)-1 ;
                    xlabel(Ax(j+1),'Porosity(Fraction)') ;
                end
            end
        end
        if FigureFlag
            Ax(j+2) = subplot(1,NumberOfHoles(i)+2,NumberOfHoles(i)+2) ; hold(Ax(j+2),'on') ; box(Ax(j+2),'on') ;
            plot(Ax(j+2),YT,DT,'Marker','o','MarkerFaceColor','g','MarkerSize',7,'MarkerEdgeColor','k','LineStyle','none') ;
            scatter(Ax(j+2),predict(net,XTrain),DTrain,23,'Marker','o','MarkerFaceColor',[0.1 0.1 0.9],'MarkerEdgeColor',[0.9370 0.9680 0.0190],'MarkerFaceAlpha',0.8) ;
            scatter(Ax(j+2),predict(net,XTest),DTest,23,'Marker','o','MarkerFaceColor',[0.9 0.1 0.1],'MarkerEdgeColor',[0.9290 0.6940 0.1250],'MarkerFaceAlpha',0.8) ;
            
            Err = mape(YT,double(predict(net,XT)))*100 ; % 0 is good 100 is bad.
            Err(Err>100) = 100 ;
    
        %     rmse(DataStruct.(WellNames{i}).Core_Properties.PHIE,evalfis(Fuzzy,XTrain))*100
        %     mape(DataStruct.(WellNames{i}).Core_Properties.PHIE,evalfis(Fuzzy,XTrain))*100
        %     mae(DataStruct.(WellNames{i}).Core_Properties.PHIE,evalfis(Fuzzy,XTrain))*100
        %     rsquared(DataStruct.(WellNames{i}).Core_Properties.PHIE,evalfis(Fuzzy,XTrain))*100
               
            Ax(j+2).XAxis.Limits(2) = 0.4 ; Ax(j+2).XAxis.Limits(1) = 0 ;
            Ax(j+2).YAxis.Limits(2) = Sec_Range(2)+1 ; Ax(j+2).YAxis.Limits(1) = Sec_Range(1)-1 ;
            title(Ax(j+2),['Core Error_{MAPE}: ' num2str(Err,'%2.2f') '%'],'FontSize',7) ; Ax(j+2).YDir = 'reverse' ;
            Ax(j+2).YAxisLocation = 'right' ;
            ylabel(Ax(j+2),'Depth(m)') ; xlabel(Ax(j+2),'Porosity(Fraction)') ;
            
            try 
                legend(Ax(j+1),[h1 , h2 , h3 , h4 , h5] , {'Log Data' , 'Predict Log' , 'Core Data' , 'Predict Core Train' , 'Predict Core Test'})
            catch
            end
            % Validation
            figure('Color','w','Name',[WellNames{i} ' , ' WantStruct ' , Cross Validation'] , 'NumberTitle','off')
            subplot(2,2,1) ; plot(predict(net,XTrain),YTrain,'Marker','o','MarkerFaceColor',[0.1 0.1 0.9],'LineStyle','none','MarkerEdgeColor','none') ; hold on ; plot([0 0.5] , [0 0.5] , 'k' , 'LineWidth' , 2) ; ylabel("Actual Porosity with Training Data") ; xlabel("Predict Porosity with Training Data")
            title(['Training Data R^{2}: ' num2str(rsquared(YTrain,predict(net,XTrain)),'%2.2f')])
        
            subplot(2,2,2) ; plot(predict(net,XTest),YTest,'Marker','o','MarkerFaceColor',[0.9 0.1 0.1],'LineStyle','none','MarkerEdgeColor','none') ; hold on ; plot([0 0.5] , [0 0.5] , 'k' , 'LineWidth' , 2) ; ylabel("Actual Porosity with Testing Data") ; xlabel("Predict Porosity with Testing Data")
            title(['Testing Data R^{2}: ' num2str(rsquared(YTest,predict(net,XTest)),'%2.2f')])
        
            subplot(2,2,3) ; plot(predict(net,XT),YT,'Marker','o','MarkerFaceColor','g','LineStyle','none','MarkerEdgeColor','none') ; hold on ; plot([0 0.5] , [0 0.5] , 'k' , 'LineWidth' , 2) ; ylabel("Actual Porosity with All Data") ; xlabel("Predict Porosity with All Data")
            title(['All Data R^{2}: ' num2str(rsquared(YT,predict(net,XT)),'%2.2f')])
        end
        SVMModel.(WantStruct)(i).SVM = net ;
        SVMModel.(WantStruct)(i).Error = rsquared(YT,predict(net,XT)) ;
    end
end

%%
M = [] ;
C = fieldnames(SVMModel) ;

for k = 1:length(fieldnames(SVMModel))
    M = [M extractfield(SVMModel.(C{k}),'Error')'] ;
end
MeanM = mean(M,1)

%% ------------ Functions
function R2 = rsquared(ActualVal,EstimateVal)
    if size(ActualVal) == size(EstimateVal) & isvector(ActualVal)
        RSS = sum((ActualVal - EstimateVal).^2) ;
        TSS = sum((ActualVal - mean(ActualVal)).^2) ;
        R2 = 1 - (RSS./TSS) ;
    else
        E = [] ;
    end
end

function E = rmse(ActualVal,EstimateVal)
    if size(ActualVal) == size(EstimateVal) & isvector(ActualVal)
        E = sqrt(sum(((ActualVal - EstimateVal).^2) ./ numel(ActualVal))) ;
    else
        E = [] ;
    end
end


function E = mae(ActualVal,EstimateVal)
    if size(ActualVal) == size(EstimateVal) & isvector(ActualVal)
        E = sum(abs(ActualVal-EstimateVal)) ./ numel(ActualVal) ;
    else
        E = [] ;
    end
end


function E = mape(ActualVal,EstimateVal)
    if size(ActualVal) == size(EstimateVal) & isvector(ActualVal)
        E = sum(abs(ActualVal-EstimateVal) ./ ActualVal) ./ numel(ActualVal) ;
    else
        E = [] ;
    end
end












