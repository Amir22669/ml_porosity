clear all ; close all ; clc %% Fuzzy %%
warning off
%% Load Data
load DataFile.mat
load FuzzySturctures.mat
WellNames = DataStruct.WellNames ;
NumberOfHoles = DataStruct.NumberOfHoles ;

%% Initialize
OptionsTune = tunefisOptions ;
OptionsTune.Method = 'particleswarm' ;
OptionsTune.OptimizationType = 'learning' ;
OptionsTune.Display = 'none' ;
OptionsTune.UseParallel = false ; 
OptionsTune.MethodOptions.MaxIterations = 5 ;


%%
for i = 6%:length(WellNames)


    % from Core properties and  porosity
    DataStruct.(WellNames{i}).Core_Properties.PHIE = DataStruct.(WellNames{i}).Core_Porosity ;
    try
        DataStruct.(WellNames{i}).Core_Properties.DT = [] ; % Remove DT
    catch
    end
    DataStruct.(WellNames{i}).Core_Properties = rmmissing(DataStruct.(WellNames{i}).Core_Properties) ; % Remove NaN Rows
    
    N = table2array(DataStruct.(WellNames{i}).Core_Properties) ; 
    N(N==0) = 1e-4 ;
    DataStruct.(WellNames{i}).Core_Properties = array2table(N,"VariableNames",DataStruct.(WellNames{i}).Core_Properties.Properties.VariableNames) ;
    clear N

    DT = DataStruct.(WellNames{i}).Core_Properties.DEPTH ;
    YT = DataStruct.(WellNames{i}).Core_Properties.PHIE ;
    XT = [DataStruct.(WellNames{i}).Core_Properties.BS DataStruct.(WellNames{i}).Core_Properties.CALI DataStruct.(WellNames{i}).Core_Properties.GR DataStruct.(WellNames{i}).Core_Properties.NPHI DataStruct.(WellNames{i}).Core_Properties.RHOB DataStruct.(WellNames{i}).Core_Properties.DRHO] ;
    
    Sec_Range = [min(DT) max(DT)] ;

    

    % 70-30 Holdout validation
    CV = cvpartition(length(YT),'Holdout',0.3) ;
    indextrain = training(CV) ;
    indextest = test(CV) ;
    XTest = XT(indextest,:) ;
    YTest = YT(indextest,:) ;
    DTest = DT(indextest,:) ;
    XTrain = XT(indextrain,:) ;
    YTrain = YT(indextrain,:) ;
    DTrain = DT(indextrain,:) ;
    
    % Scratched Data in fuzzy >> Fuzzy does not accept same column data
    Logic = max(XT) == min(XT) ;
    if any(Logic)
        Logic_Index = find(Logic == 1) ;
        for o = 1:sum(Logic)
            mu = 0 ; sigma = 0.00001 ;
            GD = gmdistribution(mu,sigma) ; Q = GD.random(length(YT))*sigma ;
            XT(:,Logic_Index(o)) = XT(:,Logic_Index(o)) + Q ;
            clear Q mu sigma o
        end
    end

    for k = 1%:length(Structures)
        clear Fuzzy Tree
        WantStruct = Structures{k} ;
        figure('Color','w','Name',[WellNames{i} ' , ' WantStruct] , 'NumberTitle','off')
        while true
            try
                disp('work on it.')
                [Tree , NumOfRulesForTune] = GenFis(XT,YT,WantStruct,0.05,4) ;

                disp('done.')
                break
            catch
            end
        end
    
        OptionsTune.NumMaxRules = NumOfRulesForTune ;
        Fuzzy = tunefis(Tree,[],XTrain,YTrain,OptionsTune) ;
    
     
        for j = 0:NumberOfHoles(i)
            Ax(j+1) = subplot(1,NumberOfHoles(i)+2,j+1) ; Ax(j+1).XColor = 'none' ; Ax(j+1).YColor = 'none' ;
            
            try 
                DataStruct.(WellNames{i}).(['Log_' num2str(j)]).DT = [] ; % Remove DT
            catch
            end
    
            DataStruct.(WellNames{i}).(['Log_' num2str(j)]) = rmmissing(DataStruct.(WellNames{i}).(['Log_' num2str(j)])) ;
            
            N = table2array(DataStruct.(WellNames{i}).(['Log_' num2str(j)])) ; 
            N(N==0) = 1e-4 ;
            DataStruct.(WellNames{i}).(['Log_' num2str(j)]) = array2table(N,"VariableNames",DataStruct.(WellNames{i}).(['Log_' num2str(j)]).Properties.VariableNames) ;
            clear N
    
            if ~isempty(DataStruct.(WellNames{i}).(['Log_' num2str(j)]))
                Ax(j+1).XColor = 'black' ; Ax(j+1).YColor = 'black' ;
                
                XL = [DataStruct.(WellNames{i}).(['Log_' num2str(j)]).BS DataStruct.(WellNames{i}).(['Log_' num2str(j)]).CALI DataStruct.(WellNames{i}).(['Log_' num2str(j)]).GR DataStruct.(WellNames{i}).(['Log_' num2str(j)]).NPHI DataStruct.(WellNames{i}).(['Log_' num2str(j)]).RHOB DataStruct.(WellNames{i}).(['Log_' num2str(j)]).DRHO] ;
                YL = DataStruct.(WellNames{i}).(['Log_' num2str(j)]).PHIE ;
                DL = DataStruct.(WellNames{i}).(['Log_' num2str(j)]).DEPTH ;
    
                
                h1 = plot(Ax(j+1),YL,DL,'g','LineWidth',2) ; Ax(j+1).YDir = 'reverse' ; hold(Ax(j+1),'on') ;
                h2 = plot(Ax(j+1),evalfis(Fuzzy,XL),DL,'b','LineWidth',2) ;
                h3 = plot(Ax(j+1),YT,DT,'Marker','o','MarkerFaceColor','g','MarkerSize',7,'MarkerEdgeColor','k','LineStyle','none') ;
                h4 = scatter(Ax(j+1),evalfis(Fuzzy,XTrain),DTrain,23,'Marker','o','MarkerFaceColor',[0.1 0.1 0.9],'MarkerEdgeColor',[0.9370 0.9680 0.0190],'MarkerFaceAlpha',0.8) ;
                h5 = scatter(Ax(j+1),evalfis(Fuzzy,XTest),DTest,23,'Marker','o','MarkerFaceColor',[0.9 0.1 0.1],'MarkerEdgeColor',[0.9290 0.6940 0.1250],'MarkerFaceAlpha',0.8) ;
    
                Err = rsquared(YL,evalfis(Fuzzy,XL)) ;
    
                title(Ax(j+1),['Log R^{2}: ' num2str(Err,'%2.2f')],'FontSize',9)
                Ax(j+1).XAxis.Limits(2) = 0.4 ; Ax(j+1).XAxis.Limits(1) = 0 ;
                Ax(j+1).YAxis.Limits(2) = Sec_Range(2)+1 ; Ax(j+1).YAxis.Limits(1) = Sec_Range(1)-1 ;
                xlabel(Ax(j+1),'Porosity(Fraction)') ;
            end
        end
    
        Ax(j+2) = subplot(1,NumberOfHoles(i)+2,NumberOfHoles(i)+2) ; hold(Ax(j+2),'on') ; box(Ax(j+2),'on') ;
        plot(Ax(j+2),YT,DT,'Marker','o','MarkerFaceColor','g','MarkerSize',7,'MarkerEdgeColor','k','LineStyle','none') ;
        scatter(evalfis(Fuzzy,XTrain),DTrain,23,'Marker','o','MarkerFaceColor',[0.1 0.1 0.9],'MarkerEdgeColor',[0.9370 0.9680 0.0190],'MarkerFaceAlpha',0.8) ;
        scatter(Ax(j+2),evalfis(Fuzzy,XTest),DTest,23,'Marker','o','MarkerFaceColor',[0.9 0.1 0.1],'MarkerEdgeColor',[0.9290 0.6940 0.1250],'MarkerFaceAlpha',0.8) ;
    
        Err = rsquared(YT,evalfis(Fuzzy,XT)) ; % 0 is good 100 is bad.
    
    
        Ax(j+2).XAxis.Limits(2) = 0.4 ; Ax(j+2).XAxis.Limits(1) = 0 ;
        Ax(j+2).YAxis.Limits(2) = Sec_Range(2)+1 ; Ax(j+2).YAxis.Limits(1) = Sec_Range(1)-1 ;
        title(Ax(j+2),['Core R^{2}: ' num2str(Err,'%2.2f')],'FontSize',9) ; Ax(j+2).YDir = 'reverse' ;
        Ax(j+2).YAxisLocation = 'right' ;
        ylabel(Ax(j+2),'Depth(m)') ; xlabel(Ax(j+2),'Porosity(Fraction)') ;
    
    
        try 
            legend(Ax(j+1),[h1 , h2 , h3 , h4 , h5] , {'Log Data' , 'Predict Log' , 'Core Data' , 'Predict Core Train' , 'Predict Core Test'})
        catch 
        end

        figure('Color','w','Name',[WellNames{i} ' , ' WantStruct ' , Cross Validation'] , 'NumberTitle','off')
        subplot(2,2,1) ; plot(evalfis(Fuzzy,XTrain),YTrain,'Marker','o','MarkerFaceColor',[0.1 0.1 0.9],'LineStyle','none','MarkerEdgeColor','none') ; hold on ; plot([0 0.5] , [0 0.5] , 'k' , 'LineWidth' , 2) ; ylabel("Actual Porosity with Training Data") ; xlabel("Predict Porosity with Training Data")
        title(['Training Data R^{2}: ' num2str(rsquared(YTrain,evalfis(Fuzzy,XTrain)),'%2.2f')])
        axis([0 0.5 0 0.5])

        subplot(2,2,2) ; plot(evalfis(Fuzzy,XTest),YTest,'Marker','o','MarkerFaceColor',[0.9 0.1 0.1],'LineStyle','none','MarkerEdgeColor','none') ; hold on ; plot([0 0.5] , [0 0.5] , 'k' , 'LineWidth' , 2) ; ylabel("Actual Porosity with Testing Data") ; xlabel("Predict Porosity with Testing Data")
        title(['Testing Data R^{2}: ' num2str(rsquared(YTest,evalfis(Fuzzy,XTest)),'%2.2f')])
        axis([0 0.5 0 0.5])

        subplot(2,2,3) ; plot(evalfis(Fuzzy,XT),YT,'Marker','o','MarkerFaceColor','g','LineStyle','none','MarkerEdgeColor','none') ; hold on ; plot([0 0.5] , [0 0.5] , 'k' , 'LineWidth' , 2) ; ylabel("Actual Porosity with All Data") ; xlabel("Predict Porosity with All Data") 
        title(['All Data R^{2}: ' num2str(rsquared(YT,evalfis(Fuzzy,XT)),'%2.2f')])
        axis([0 0.5 0 0.5])

        FuzzyModel.(WantStruct)(i).Fuzzy = Fuzzy ;
        FuzzyModel.(WantStruct)(i).Error = rsquared(YTest,evalfis(Fuzzy,XTest)) ;
    end
end

%% Mean error of structures
M = [] ;
C = fieldnames(FuzzyModel) ;
for k = 1:length(fieldnames(FuzzyModel))
    M = [M extractfield(FuzzyModel.(C{k}),'Error')'] ;
end
MeanM = mean(M,1)
%% 
for k = 1:length(fieldnames(FuzzyModel))
    figure('Color','w','Name',C{k},'NumberTitle','off')
    plotfis(FuzzyModel.(C{k})(i).Fuzzy,Legend="on")
    F = gcf ; F.Name = C{k} ;
end

%     Close, opened figures
    close all
%% ------------ Functions
function R2 = rsquared(ActualVal,EstimateVal)
    if size(ActualVal) == size(EstimateVal) & isvector(ActualVal)
        RSS = sum((ActualVal - EstimateVal).^2) ;
        TSS = sum((ActualVal - mean(ActualVal)).^2) ;
        R2 = 1 - (RSS./TSS) ;
    else
        E = [] ;
    end
end

function E = rmse(ActualVal,EstimateVal)
    if size(ActualVal) == size(EstimateVal) & isvector(ActualVal)
        E = sqrt(sum(((ActualVal - EstimateVal).^2) ./ numel(ActualVal))) ;
    else
        E = [] ;
    end
end


function E = mae(ActualVal,EstimateVal)
    if size(ActualVal) == size(EstimateVal) & isvector(ActualVal)
        E = sum(abs(ActualVal-EstimateVal)) ./ numel(ActualVal) ;
    else
        E = [] ;
    end
end


function E = mape(ActualVal,EstimateVal)
    if size(ActualVal) == size(EstimateVal) & isvector(ActualVal)
        E = sum(abs(ActualVal-EstimateVal) ./ ActualVal) ./ numel(ActualVal) ;
    else
        E = [] ;
    end
end

function [GenTree , NOMR] = GenFis(XTrain,YTrain,Struct,EFC,RepCol)
    % EFC = ErrorFindingCategories
    switch Struct
        case 'Struct1'
            % Find related data
            G = DataGraph(XTrain,YTrain,EFC) ;
            % plot(G) ;
            MF = max(G.conncomp) ;

            Options = genfisOptions('FCMClustering','FISType','mamdani','Verbose',0,'NumClusters',MF,'Exponent',1.5) ;
            Options.MinImprovement = 1e-2 ; %Option for options>> Error for separete clusters
            Options.MaxNumIteration = 10 ;
            
            GenFuzzy = genfis(XTrain,YTrain,Options) ;
            
            % Link date
            id = DataConnect(XTrain,G,MF) ;

            % Make Membership functions
            [GenFuzzy , n] = DataMF(GenFuzzy,XTrain,YTrain,MF,id) ;
  
            % Make rules
            GenFuzzy = DataRules(GenFuzzy,XTrain,YTrain,MF,id,n) ;
            
            % Con1
            Con1 = [] ;

            GenTree = fistree([GenFuzzy],Con1);
            NOMR = length(GenTree.FIS(end).Rules)+1  ;

        case 'Struct2'
            G = DataGraph(XTrain,YTrain,EFC) ;
            MF = max(G.conncomp) ;
            Options = genfisOptions('FCMClustering','FISType','mamdani','Verbose',0,'NumClusters',MF,'Exponent',1.5) ;
            Options.MinImprovement = 1e-2 ;
            Options.MaxNumIteration = 10 ;
            GenFuzzy1 = genfis(XTrain,YTrain,Options) ;
            id = DataConnect(XTrain,G,MF) ;
            [GenFuzzy1 , n] = DataMF(GenFuzzy1,XTrain,YTrain,MF,id) ;
            GenFuzzy1 = DataRules(GenFuzzy1,XTrain,YTrain,MF,id,n) ;
            
            XTrain2 = [evalfis(GenFuzzy1,XTrain) XTrain(:,RepCol)] ;
            G = DataGraph(XTrain2,YTrain,EFC) ;
            MF = max(G.conncomp) ;
            Options.NumClusters = MF ;
            GenFuzzy2 = genfis(XTrain2,YTrain,Options) ;
            id = DataConnect(XTrain2,G,MF) ;
            [GenFuzzy2 , n] = DataMF(GenFuzzy2,XTrain2,YTrain,MF,id) ;
            GenFuzzy2 = DataRules(GenFuzzy2,XTrain2,YTrain,MF,id,n) ;
            
            % Con1
            Con1 = [string([char(GenFuzzy1.Name) '/' char(GenFuzzy1.Outputs.Name)]) , string([char(GenFuzzy2.Name) '/' char(GenFuzzy2.Inputs(1).Name)])] ;
            
            % Con2
            Con2 = [string([char(GenFuzzy1.Name) '/' char(GenFuzzy1.Inputs(RepCol).Name)]) , string([char(GenFuzzy2.Name) '/' char(GenFuzzy2.Inputs(2).Name)])] ;
            
            GenTree = fistree([GenFuzzy1 GenFuzzy2],[Con1 ; Con2]);
            NOMR = max([length(GenTree.FIS(1).Rules) length(GenTree.FIS(2).Rules)])+1 ;

        case 'Struct3'
            G = DataGraph(XTrain,YTrain,EFC) ;
            MF = max(G.conncomp) ;
            Options = genfisOptions('FCMClustering','FISType','mamdani','Verbose',0,'NumClusters',MF,'Exponent',1.5) ;
            Options.MinImprovement = 1e-2 ;
            Options.MaxNumIteration = 10 ;
            GenFuzzy1 = genfis(XTrain,YTrain,Options) ;
            id = DataConnect(XTrain,G,MF) ;
            [GenFuzzy1 , n] = DataMF(GenFuzzy1,XTrain,YTrain,MF,id) ;
            GenFuzzy1 = DataRules(GenFuzzy1,XTrain,YTrain,MF,id,n) ;
            
            XTrain2 = [XTrain(:,1:RepCol-1) XTrain(:,RepCol+1:end)] ;
            G = DataGraph(XTrain2,YTrain,EFC) ;
            MF = max(G.conncomp) ;
            Options.NumClusters = MF ;
            GenFuzzy2 = genfis(XTrain2,YTrain,Options) ;
            id = DataConnect(XTrain2,G,MF) ;
            [GenFuzzy2 , n] = DataMF(GenFuzzy2,XTrain2,YTrain,MF,id) ;
            GenFuzzy2 = DataRules(GenFuzzy2,XTrain2,YTrain,MF,id,n) ;
            
            XTrain3 = [evalfis(GenFuzzy1,XTrain) evalfis(GenFuzzy2,XTrain2)] ;
            G = DataGraph(XTrain3,YTrain,EFC/2) ;
            MF = max(G.conncomp) ;
            Options.NumClusters = MF ;
            GenFuzzy3 = genfis(XTrain3,YTrain,Options) ;
            id = DataConnect(XTrain3,G,MF) ;
            [GenFuzzy3 , n] = DataMF(GenFuzzy3,XTrain3,YTrain,MF,id) ;
            GenFuzzy3 = DataRules(GenFuzzy3,XTrain3,YTrain,MF,id,n) ;
            
            % Con1
            Con1 = [string([char(GenFuzzy1.Name) '/' char(GenFuzzy1.Outputs.Name)]) , string([char(GenFuzzy3.Name) '/' char(GenFuzzy3.Inputs(1).Name)]) ; string([char(GenFuzzy2.Name) '/' char(GenFuzzy2.Outputs.Name)]) , string([char(GenFuzzy3.Name) '/' char(GenFuzzy3.Inputs(2).Name)])] ;
            
            % Con2
            for i = 1:size(XTrain,2)-length(RepCol)
                Con2(i,:) = [string([char(GenFuzzy1.Name) '/' char(GenFuzzy1.Inputs(i).Name)]) , string([char(GenFuzzy2.Name) '/' char(GenFuzzy2.Inputs(i).Name)])] ;
                if i >= RepCol
                    Con2(i,:) = [string([char(GenFuzzy1.Name) '/' char(GenFuzzy1.Inputs(i+1).Name)]) , string([char(GenFuzzy2.Name) '/' char(GenFuzzy2.Inputs(i).Name)])] ;
                end
            end
    
            GenTree = fistree([GenFuzzy1 GenFuzzy2 GenFuzzy3],[Con1 ; Con2]);
            NOMR = max([length(GenTree.FIS(1).Rules) length(GenTree.FIS(2).Rules) length(GenTree.FIS(3).Rules)])+1 ;

        case 'Struct4'
            G = DataGraph(XTrain,YTrain,EFC) ;
            MF = max(G.conncomp) ; 
            Options = genfisOptions('FCMClustering','FISType','mamdani','Verbose',0,'NumClusters',MF,'Exponent',1.5) ;
            Options.MinImprovement = 1e-2 ;
            Options.MaxNumIteration = 10 ;
            GenFuzzy1 = genfis(XTrain,YTrain,Options) ;
            id = DataConnect(XTrain,G,MF) ; 
            [GenFuzzy1 , n] = DataMF(GenFuzzy1,XTrain,YTrain,MF,id) ;
            GenFuzzy1 = DataRules(GenFuzzy1,XTrain,YTrain,MF,id,n) ;

            XTrain2 = [XTrain(:,1:RepCol-1) XTrain(:,RepCol+1:end)] ;
            G = DataGraph(XTrain2,YTrain,EFC) ;
            MF = max(G.conncomp) ;
            Options.NumClusters = MF ;
            GenFuzzy2 = genfis(XTrain2,YTrain,Options) ;
            id = DataConnect(XTrain2,G,MF) ;
            [GenFuzzy2 , n] = DataMF(GenFuzzy2,XTrain2,YTrain,MF,id) ;
            GenFuzzy2 = DataRules(GenFuzzy2,XTrain2,YTrain,MF,id,n) ;

            XTrain3 = [evalfis(GenFuzzy1,XTrain) evalfis(GenFuzzy2,XTrain2)] ;
            G = DataGraph(XTrain3,YTrain,EFC/2) ;
            MF = max(G.conncomp) ;
            Options.NumClusters = MF ;
            GenFuzzy3 = genfis(XTrain3,YTrain,Options) ;
            id = DataConnect(XTrain3,G,MF) ;
            [GenFuzzy3 , n] = DataMF(GenFuzzy3,XTrain3,YTrain,MF,id) ;
            GenFuzzy3 = DataRules(GenFuzzy3,XTrain3,YTrain,MF,id,n) ;
            

            XTrain4 = [evalfis(GenFuzzy3,XTrain3) XTrain(:,RepCol)] ;
            G = DataGraph(XTrain4,YTrain,EFC/4) ;
            MF = max(G.conncomp) ;
            Options.NumClusters = MF ;
            GenFuzzy4 = genfis(XTrain4,YTrain,Options) ;
            id = DataConnect(XTrain4,G,MF) ;
            [GenFuzzy4 , n] = DataMF(GenFuzzy4,XTrain4,YTrain,MF,id) ;
            GenFuzzy4 = DataRules(GenFuzzy4,XTrain4,YTrain,MF,id,n) ;
            GenFuzzy4.Name = "mamdani21-1" ;

            % Con1
            Con1 = [string([char(GenFuzzy1.Name) '/' char(GenFuzzy1.Outputs.Name)]) , string([char(GenFuzzy3.Name) '/' char(GenFuzzy3.Inputs(1).Name)]) ; string([char(GenFuzzy2.Name) '/' char(GenFuzzy2.Outputs.Name)]) , string([char(GenFuzzy3.Name) '/' char(GenFuzzy3.Inputs(2).Name)])] ;
            
            % Con2
            for i = 1:size(XTrain,2)-length(RepCol)
                Con2(i,:) = [string([char(GenFuzzy1.Name) '/' char(GenFuzzy1.Inputs(i).Name)]) , string([char(GenFuzzy2.Name) '/' char(GenFuzzy2.Inputs(i).Name)])] ;
                if i >= RepCol
                    Con2(i,:) = [string([char(GenFuzzy1.Name) '/' char(GenFuzzy1.Inputs(i+1).Name)]) , string([char(GenFuzzy2.Name) '/' char(GenFuzzy2.Inputs(i).Name)])] ;
                end
            end

            % Con3
            Con3 = [string([char(GenFuzzy3.Name) '/' char(GenFuzzy3.Outputs.Name)]) , string([char(GenFuzzy4.Name) '/' char(GenFuzzy4.Inputs(1).Name)]) ; string([char(GenFuzzy1.Name) '/' char(GenFuzzy1.Inputs(RepCol).Name)]) , string([char(GenFuzzy4.Name) '/' char(GenFuzzy4.Inputs(2).Name)])] ;
            
            GenTree = fistree([GenFuzzy1 GenFuzzy2 GenFuzzy3 GenFuzzy4],[Con1 ; Con2 ; Con3]);
            NOMR = max([length(GenTree.FIS(1).Rules) length(GenTree.FIS(2).Rules) length(GenTree.FIS(3).Rules) length(GenTree.FIS(4).Rules)])+1 ;

    end
end



function G = DataGraph(XTrain,YTrain,EFC)
    for new = 1:length(YTrain)
        for ii = 1:size(XTrain,1)
            KO(ii,new) = max(abs(([XTrain(new,:) YTrain(new,1)] - [XTrain(ii,:) YTrain(ii,1)]) ./ max([XTrain YTrain]))) ;
        end
    end
    GO = KO<EFC & KO~=0 ;
    G = graph(GO) ;
end

function id = DataConnect(XTrain,G,MF)
    id(:,size(XTrain,2)+1) = G.conncomp' ;
    for n = 1:size(XTrain,2)
        id(:,n) = kmeans(XTrain(:,n),MF) ;
    end
end

function [GenFuzzy , n] = DataMF(GenFuzzy,XTrain,YTrain,MF,id)
    for n = 1:size(XTrain,2)
        for m = 1:MF
            GenFuzzy.Inputs(1,n).MembershipFunctions(1,m).Type = 'gauss2mf' ;
            GenFuzzy.Inputs(1,n).MembershipFunctions(1,m).Parameters = [ExpFunc(mean(XTrain(:,n))) min(XTrain(id(:,n)==m,n)) ExpFunc(mean(XTrain(:,n))) max(XTrain(id(:,n)==m,n))] ;
        end
    end
    for m = 1:MF
        GenFuzzy.Outputs.MembershipFunctions(1,m).Type = 'gauss2mf' ;
        GenFuzzy.Outputs.MembershipFunctions(1,m).Parameters = [ExpFunc(mean(YTrain)) min(YTrain(id(:,n+1)==m)) ExpFunc(mean(YTrain)) max(YTrain(id(:,n+1)==m))] ;
    end
end

function GenFuzzy = DataRules(GenFuzzy,XTrain,YTrain,MF,id,n)
    for kk = 1:MF
        for nn = 1:size(XTrain,2)
            for m = 1:MF
                MeanClust(m,nn) = abs(mean([min(XTrain(id(:,n+1)==kk,nn)) max(XTrain(id(:,n+1)==kk,nn))] - [min(XTrain(id(:,nn)==m,nn)) max(XTrain(id(:,nn)==m,nn))])) ;
            end
        end
        R = [] ;
        for nn = 1:size(XTrain,2)
            [r , ~] = find(MeanClust(:,nn) == min(MeanClust(:,nn))) ; %%
            R = [R r(1)] ;
        end
        GenFuzzy.Rules(kk).Antecedent = R ;
        clear MeanClust 
        %     NR(kk,:) = R ;
    end
end


function R = ExpFunc(x)
    R = (0.001091*x^2 + 0.01348*x + 0.006988)/1.8 ;
end