classdef MachineModel
    properties
        Net = [] ;
        System = [] ;
        ExtraMachine = [] ;
        Method = [] ;
        ID = [] ;
        XT = [] ;
        MF = [] ;
    end
    methods
        function f = ExportMachine(obj,x)
            switch class(obj.ExtraMachine)
                case 'network'
                    Expo = @(ExtraMachine,X) (ExtraMachine(X'))' ;
                otherwise
                    Expo = @(ExtraMachine,X) predict(ExtraMachine,X) ;
            end
            switch obj.Method
                case 1
                    X = Data2DtoDataCNN(x,obj.ID,obj.XT,obj.MF) ;
                    f = evalfis(obj.System,double(predict(obj.Net,X))) ;
                case 2
                    newx = [x evalfis(obj.System,x)] ;
                    X = Data2DtoDataCNN(newx,obj.ID,obj.XT,obj.MF) ;
                    f = double(predict(obj.Net,X)) ;
                case 11
                    newx = [x Expo(obj.ExtraMachine,x)] ;
                    X = Data2DtoDataCNN(newx,obj.ID,obj.XT,obj.MF) ;
                    v = double(predict(obj.Net,X)) ;
                    f = evalfis(obj.System,v) ;
                case 21
                    newx = Expo(obj.ExtraMachine,x) ;
                    v = [x evalfis(obj.System,newx)] ;
                    X = Data2DtoDataCNN(v,obj.ID,obj.XT,obj.MF) ;
                    f = double(predict(obj.Net,X)) ;
                case 12
                    X = Data2DtoDataCNN(x,obj.ID,obj.XT,obj.MF) ;
                    newx = double(predict(obj.Net,X)) ;
                    v = Expo(obj.ExtraMachine,newx) ;
                    f = evalfis(obj.System,v) ;
                case 22
                    v = evalfis(obj.System,x) ;
                    newx = [x Expo(obj.ExtraMachine,v)] ;
                    X = Data2DtoDataCNN(newx,obj.ID,obj.XT,obj.MF) ;
                    f = double(predict(obj.Net,X)) ;
                case 13
                    X = Data2DtoDataCNN(x,obj.ID,obj.XT,obj.MF) ;
                    newx = double(predict(obj.Net,X)) ;
                    v = evalfis(obj.System,newx) ;
                    f = Expo(obj.ExtraMachine,v) ;
                case 23
                    v = evalfis(obj.System,x) ;
                    X = Data2DtoDataCNN([x v],obj.ID,obj.XT,obj.MF) ;
                    newx = double(predict(obj.Net,X)) ;
                    f = Expo(obj.ExtraMachine,newx) ;
            end
        end
        function Solver = ExportSolver(obj)
            Solver = @(x) ExportMachine(obj,x) ;
        end
    end
end