clear ; close all ; clc %% CNN %%
warning off
%%
load DataFile.mat
WellNames = DataStruct.WellNames ;
%% https://www.mathworks.com/help/deeplearning/ug/list-of-deep-learning-layers.html#mw_ba2587f2-c877-4d5c-b500-92762cb4af82
Structural = {'MATLAB'} ; % CNN
Structures = {'Struct1'} ; % Fuzzy
Kernel = {'LaplaceRBF'} ; % SVM % linear LaplaceRBF
Network = {'FeedForward'} ; % ANN

FigureFlag = false ;
% type:1 CNN>>Fuzzy ,
% type:2 Fuzzy>>CNN ,
% type:11 AddMachine>>CNN>>Fuzzy ,
% type:21 AddMachine>>Fuzzy>>CNN , 
% type:12 CNN>>AddMachine>>Fuzzy ,
% type:22 Fuzzy>>AddMachine>>CNN ,
% type:13 CNN>>Fuzzy>>AddMachine ,
% type:23 Fuzzy>>CNN>>AddMachine ,
Combine = 23 ; 
AdditionalMachine = 1 ; % 1 is SVM , 2 is ANN
%=======================================================================
if AdditionalMachine == 1
    WantStructAM = Kernel{1} ;
    if (Combine == 13) | (Combine == 23)
       WantStructAM = 'linear' ;  
    end
    AddMachineExporter = @(ExtraMachine,x) predict(ExtraMachine,x) ;
else
    WantStructAM = Network{1} ;
    AddMachineExporter = @(ExtraMachine,x) (ExtraMachine(x'))' ;
end

%%
for i = 6%:length(WellNames)

    % from Core properties and porosity
    DataStruct.(WellNames{i}).Core_Properties.PHIE = DataStruct.(WellNames{i}).Core_Porosity ;
    try
        DataStruct.(WellNames{i}).Core_Properties.DT = [] ; % Remove DT
    catch
    end
    DataStruct.(WellNames{i}).Core_Properties = rmmissing(DataStruct.(WellNames{i}).Core_Properties) ; % Remove NaN Rows
% 
    N = table2array(DataStruct.(WellNames{i}).Core_Properties) ;
    N(N==0) = 1e-4 ;
    DataStruct.(WellNames{i}).Core_Properties = array2table(N,"VariableNames",DataStruct.(WellNames{i}).Core_Properties.Properties.VariableNames) ;
    clear N
% 
    DT = DataStruct.(WellNames{i}).Core_Properties.DEPTH ;
    YT = 100 * DataStruct.(WellNames{i}).Core_Properties.PHIE ; % fraction to percent
    XT = [DataStruct.(WellNames{i}).Core_Properties.BS DataStruct.(WellNames{i}).Core_Properties.CALI DataStruct.(WellNames{i}).Core_Properties.GR DataStruct.(WellNames{i}).Core_Properties.NPHI DataStruct.(WellNames{i}).Core_Properties.RHOB DataStruct.(WellNames{i}).Core_Properties.DRHO] ;

    GeneralMatrix = sortrows([DT XT YT],1) ;
    DT = GeneralMatrix(:,1) ;
    XT = GeneralMatrix(:,2:end-1) ;
    YT = GeneralMatrix(:,end) ;

    Sec_Range = [min(DT) max(DT)] ;
% 
    Logic = max(XT) == min(XT) ;
    if any(Logic)
        Logic_Index = find(Logic == 1) ;
        for o = 1:sum(Logic)
            mu = 0 ; sigma = 0.00001 ;
            GD = gmdistribution(mu,sigma) ; Q = GD.random(length(YT))*sigma ;
            XT(:,Logic_Index(o)) = XT(:,Logic_Index(o)) + Q ;
            clear Q mu sigma o
        end
    end

    HyperParameter = 0.001 ; % Hyper-Parameter to Clustering
    switch Combine
        case 1 % CNN >> Fuzzy
            % Data to start CNN
            [G,MF,Dim,ID,XT,YT,DT,XTrain,YTrain,DTrain,XTest,YTest,DTest,xt,xtrain,xtest,~,~] = PrepareData(XT,YT,DT,0.3,HyperParameter) ;
            
            % CNN
            WantStruct = Structural{1} ;
            Net = CNNModel(xt,YT,xtest,YTest,Dim,WantStruct) ;
            
            % Fuzzy
            WantStruct = Structures{1} ;
            SDControl = 'dive' ;
            System = FuzzySystem(double(predict(Net,xt)),YT,double(predict(Net,xtrain)),YTrain,WantStruct,HyperParameter/1000,[],SDControl) ; % "ReplaceColumn" only use in Structure 2,3,4

         
            % Combine
            M = MachineModel ; % A New Class in Directory
            M.Net = Net ;
            M.System = System ;
            M.Method = Combine ;
            M.ID = ID ;
            M.XT = XT ;
            M.MF = MF ;
            Solver = ExportSolver(M) ; % Make Solver to use
            
            % Example: 
            % Sample = XT(5:9,:)*1.000000000001 ;
            % Solver(Sample) ; 

            WantStruct = 'CNN_Fuzzy' ;
        
        case 2 % Fuzzy >> CNN
            % Data to start Other
            [G,MF,~,~,XT,YT,DT,XTrain,YTrain,DTrain,XTest,YTest,DTest,~,~,~,indextrain,indextest] = PrepareData(XT,YT,DT,0.3,HyperParameter) ;
            
            % Fuzzy
            WantStruct = Structures{1} ;
            SDControl = 'dive' ;
            System = FuzzySystem(XT,YT,XTrain,YTrain,WantStruct,HyperParameter,[],SDControl) ; % "ReplaceColumn" only use in Structure 2,3,4
            
            % Data to before CNN
            [Dim,ID,NewXT,~,~,~,~,~,~,~,~,xt,xtrain,xtest] = PrepareDataBeforeCNN([XT evalfis(System,XT)],YT,DT,MF,indextrain,indextest) ;
            
            % CNN
            WantStruct = Structural{1} ;
            Net = CNNModel(xt,YT,xtest,YTest,Dim,WantStruct) ;

            % Combine
            M = MachineModel ; % A New Class in Directory
            M.Net = Net ;
            M.System = System ;
            M.Method = Combine ;
            M.ID = ID ;
            M.XT = NewXT ;
            M.MF = MF ;
            Solver = ExportSolver(M) ; % Make Solver to use
            
            % Example: 
            % Sample = XT(5:9,:)*1.000000000001 ;
            % Solver(Sample) ;

            WantStruct = 'Fuzzy_CNN' ;

        case 11
            % Data to start Other
            [G,MF,~,~,XT,YT,DT,XTrain,YTrain,DTrain,XTest,YTest,DTest,~,~,~,indextrain,indextest] = PrepareData(XT,YT,DT,0.3,HyperParameter) ;

            % AdditionalMachine
            [ExtraMachine , AddStruct] = AddMachineSelector(XT,YT,WantStructAM,AdditionalMachine,0.3) ;

            % Data to before CNN
            [Dim,ID,NewXT,~,~,~,~,~,~,~,~,xt,xtrain,xtest] = PrepareDataBeforeCNN([XT AddMachineExporter(ExtraMachine,XT)],YT,DT,MF,indextrain,indextest) ;
            
            % CNN
            WantStruct = Structural{1} ;
            Net = CNNModel(xt,YT,xtest,YTest,Dim,WantStruct) ;

            % Fuzzy
            WantStruct = Structures{1} ;
            SDControl = 'dive' ;
            System = FuzzySystem(double(predict(Net,xt)),YT,double(predict(Net,xtrain)),YTrain,WantStruct,HyperParameter/1000,[],SDControl) ; % "ReplaceColumn" only use in Structure 2,3,4
            
            % Combine
            M = MachineModel ; % A New Class in Directory
            M.Net = Net ;
            M.System = System ;
            M.ExtraMachine = ExtraMachine ;
            M.Method = Combine ;
            M.ID = ID ;
            M.XT = NewXT ;
            M.MF = MF ;
            Solver = ExportSolver(M) ; % Make Solver to use

            WantStruct = [AddStruct '_CNN_Fuzzy'] ;

        case 21
            % Data to start Other
            [G,MF,~,~,XT,YT,DT,XTrain,YTrain,DTrain,XTest,YTest,DTest,~,~,~,indextrain,indextest] = PrepareData(XT,YT,DT,0.3,HyperParameter) ;

            % AdditionalMachine
            [ExtraMachine , AddStruct] = AddMachineSelector(XT,YT,WantStructAM,AdditionalMachine,0.3) ;

            % Fuzzy
            WantStruct = Structures{1} ; 
            SDControl = 'dive' ;
            System = FuzzySystem(AddMachineExporter(ExtraMachine,XT),YT,AddMachineExporter(ExtraMachine,XTrain),YTrain,WantStruct,HyperParameter/1000,[],SDControl) ; % "ReplaceColumn" only use in Structure 2,3,4
            
            % Data to before CNN
            [Dim,ID,NewXT,~,~,~,~,~,~,~,~,xt,xtrain,xtest] = PrepareDataBeforeCNN([XT evalfis(System,AddMachineExporter(ExtraMachine,XT))],YT,DT,MF,indextrain,indextest) ;
            
            % CNN
            WantStruct = Structural{1} ;
            Net = CNNModel(xt,YT,xtest,YTest,Dim,WantStruct) ;

            % Combine
            M = MachineModel ; % A New Class in Directory
            M.Net = Net ;
            M.System = System ;
            M.ExtraMachine = ExtraMachine ;
            M.Method = Combine ;
            M.ID = ID ;
            M.XT = NewXT ;
            M.MF = MF ;
            Solver = ExportSolver(M) ; % Make Solver to use

            WantStruct = [AddStruct '_Fuzzy_CNN'] ;

        case 12
            % Data to start CNN
            [G,MF,Dim,ID,XT,YT,DT,XTrain,YTrain,DTrain,XTest,YTest,DTest,xt,xtrain,xtest,~,~] = PrepareData(XT,YT,DT,0.3,HyperParameter) ;
            
            % CNN
            WantStruct = Structural{1} ;
            Net = CNNModel(xt,YT,xtest,YTest,Dim,WantStruct) ;

            % AdditionalMachine 
            [ExtraMachine , AddStruct] = AddMachineSelector(double(predict(Net,xt)),YT,WantStructAM,AdditionalMachine,0.3) ;

            % Fuzzy 
            WantStruct = Structures{1} ;
            SDControl = 'dive' ;
            System = FuzzySystem(AddMachineExporter(ExtraMachine,double(predict(Net,xt))),YT,AddMachineExporter(ExtraMachine,double(predict(Net,xtrain))),YTrain,WantStruct,HyperParameter/1000,[],SDControl) ; % "ReplaceColumn" only use in Structure 2,3,4

            % Combine
            M = MachineModel ; % A New Class in Directory
            M.Net = Net ;
            M.System = System ;
            M.ExtraMachine = ExtraMachine ;
            M.Method = Combine ;
            M.ID = ID ;
            M.XT = XT ;
            M.MF = MF ;
            Solver = ExportSolver(M) ; % Make Solver to use

            WantStruct = ['CNN_' AddStruct '_Fuzzy'] ;

        case 22
            % Data to start Other
            [G,MF,~,~,XT,YT,DT,XTrain,YTrain,DTrain,XTest,YTest,DTest,~,~,~,indextrain,indextest] = PrepareData(XT,YT,DT,0.3,HyperParameter) ;

            % Fuzzy
            WantStruct = Structures{1} ;
            SDControl = 'dive' ;
            System = FuzzySystem(XT,YT,XTrain,YTrain,WantStruct,HyperParameter,[],SDControl) ; % "ReplaceColumn" only use in Structure 2,3,4
            
            % AdditionalMachine
            [ExtraMachine , AddStruct] = AddMachineSelector(evalfis(System,XT),YT,WantStructAM,AdditionalMachine,0.3) ;
            

            % Data to before CNN 
            [Dim,ID,NewXT,~,~,~,~,~,~,~,~,xt,xtrain,xtest] = PrepareDataBeforeCNN([XT AddMachineExporter(ExtraMachine,evalfis(System,XT))],YT,DT,MF,indextrain,indextest) ;

            % CNN
            WantStruct = Structural{1} ;
            Net = CNNModel(xt,YT,xtest,YTest,Dim,WantStruct) ;

            % Combine
            M = MachineModel ; % A New Class in Directory
            M.Net = Net ;
            M.System = System ;
            M.ExtraMachine = ExtraMachine ;
            M.Method = Combine ;
            M.ID = ID ;
            M.XT = NewXT ;
            M.MF = MF ;
            Solver = ExportSolver(M) ; % Make Solver to use

            WantStruct = ['Fuzzy_' AddStruct '_CNN'] ;

        case 13
            % Data to start CNN
            [G,MF,Dim,ID,XT,YT,DT,XTrain,YTrain,DTrain,XTest,YTest,DTest,xt,xtrain,xtest,~,~] = PrepareData(XT,YT,DT,0.3,HyperParameter) ;
            
            % CNN
            WantStruct = Structural{1} ;
            Net = CNNModel(xt,YT,xtest,YTest,Dim,WantStruct) ;

            % Fuzzy
            WantStruct = Structures{1} ;
            SDControl = 'dive' ;
            System = FuzzySystem(double(predict(Net,xt)),YT,double(predict(Net,xtrain)),YTrain,WantStruct,HyperParameter/1000,[],SDControl) ; % "ReplaceColumn" only use in Structure 2,3,4

            % AdditionalMachine
            [ExtraMachine , AddStruct] = AddMachineSelector(evalfis(System,double(predict(Net,xt))),YT,WantStructAM,AdditionalMachine,0.3) ;

            % Combine
            M = MachineModel ; % A New Class in Directory
            M.Net = Net ;
            M.System = System ;
            M.ExtraMachine = ExtraMachine ;
            M.Method = Combine ;
            M.ID = ID ;
            M.XT = XT ;
            M.MF = MF ;
            Solver = ExportSolver(M) ; % Make Solver to use

            WantStruct = ['CNN_Fuzzy_' AddStruct] ;

        case 23
            % Data to start Other
            [G,MF,~,~,XT,YT,DT,XTrain,YTrain,DTrain,XTest,YTest,DTest,~,~,~,indextrain,indextest] = PrepareData(XT,YT,DT,0.3,HyperParameter) ;

            % Fuzzy
            WantStruct = Structures{1} ;
            SDControl = 'dive' ;
            System = FuzzySystem(XT,YT,XTrain,YTrain,WantStruct,HyperParameter,[],SDControl) ; % "ReplaceColumn" only use in Structure 2,3,4

            % Data to before CNN
            [Dim,ID,NewXT,~,~,~,~,~,~,~,~,xt,xtrain,xtest] = PrepareDataBeforeCNN([XT evalfis(System,XT)],YT,DT,MF,indextrain,indextest) ;
            
            % CNN
            WantStruct = Structural{1} ;
            Net = CNNModel(xt,YT,xtest,YTest,Dim,WantStruct) ;

            % AdditionalMachine
            [ExtraMachine , AddStruct] = AddMachineSelector(double(predict(Net,xt)),YT,WantStructAM,AdditionalMachine,0.3) ;

            % Combine
            M = MachineModel ; % A New Class in Directory
            M.Net = Net ;
            M.System = System ;
            M.ExtraMachine = ExtraMachine ;
            M.Method = Combine ;
            M.ID = ID ;
            M.XT = NewXT ;
            M.MF = MF ;
            Solver = ExportSolver(M) ; % Make Solver to use

            WantStruct = ['Fuzzy_CNN_' AddStruct] ;
    end


    if FigureFlag
        
        % Validation
        figure('Color','w','Name',[WellNames{i} ' , ' WantStruct ' , Cross Validation'] , 'NumberTitle','off')
        subplot(2,3,1) ; 
        plot(Solver(XTrain)/100,YTrain/100,'Marker','o','MarkerFaceColor',[0.1 0.1 0.9],'LineStyle','none','MarkerEdgeColor','none') ; hold on ; plot([0 0.5] , [0 0.5] , 'k' , 'LineWidth' , 2) ; ylabel("Actual Porosity with Training Data") ; xlabel("Predict Porosity with Training Data")
        title(['Training Data R^{2}: ' num2str(rsquared(YTrain,Solver(XTrain)),'%2.4f')])
        axis([0 0.5 0 0.5])

        subplot(2,3,2) ; 
        plot(Solver(XTest)/100,YTest/100,'Marker','o','MarkerFaceColor',[0.9 0.1 0.1],'LineStyle','none','MarkerEdgeColor','none') ; hold on ; plot([0 0.5] , [0 0.5] , 'k' , 'LineWidth' , 2) ; ylabel("Actual Porosity with Testing Data") ; xlabel("Predict Porosity with Testing Data")
        title(['Testing Data R^{2}: ' num2str(rsquared(YTest,Solver(XTest)),'%2.4f')])
        axis([0 0.5 0 0.5])

        subplot(2,3,4) ; 
        H1 = plot(Solver(XTrain)/100,YTrain/100,'Marker','o','MarkerFaceColor','b','LineStyle','none','MarkerEdgeColor','none') ; hold on ; plot([0 0.5] , [0 0.5] , 'k' , 'LineWidth' , 2) ; ylabel("Actual Porosity with All Data") ; xlabel("Predict Porosity with All Data")
        H2 = plot(Solver(XTest)/100,YTest/100,'Marker','o','MarkerFaceColor',[0.9 0.1 0.1],'LineStyle','none','MarkerEdgeColor','none') ;
        title(['All Data R^{2}: ' num2str(rsquared(YT,Solver(XT)),'%2.4f')])
        axis([0 0.5 0 0.5])

        legend([H1 , H2] , {'Training Data' , 'Testing Data'},'Location','northeastoutside') ;

        Ax = subplot(2,3,[3,6]) ; hold(Ax,'on') ;
        h1 = plot(Ax,YT/100,DT,'Color','k','LineWidth',2.5,'LineStyle','-') ;
        h2 = plot(Ax,Solver(XT)/100,DT,'Color',[0.9 0.1 0.1],'LineWidth',3.5,'LineStyle','--') ;
        Err = mape(YT/100,Solver(XT)/100)*100 ; % 0 is good, 100 is bad.
        Err(Err>100) = 100 ;


        Ax.XAxis.Limits(2) = 0.4 ; Ax.XAxis.Limits(1) = 0 ;
        Ax.YAxis.Limits(2) = Sec_Range(2)+1 ; Ax.YAxis.Limits(1) = Sec_Range(1)-1 ;
        title(Ax,['Core Error_{MAPE}: ' num2str(Err,'%2.4f') '%']) ; Ax.YDir = 'reverse' ;
        Ax.YAxisLocation = 'right' ;
        ylabel(Ax,'Depth[m]') ; xlabel(Ax,'Porosity(\phi) [Fraction]') ; box(Ax,'on')
        
        legend(Ax,[h1 , h2] , {'Core Data' , 'Core Data Predicted'},'Location','southwestoutside') ;
    end

    MachineModel.(WantStruct)(i).Machine = M ;
    MachineModel.(WantStruct)(i).Error = rsquared(YT,Solver(XT)) ;
%     disp(WantStruct)
    
end

%% Mean error of structures
U = [] ;
C = fieldnames(MachineModel) ;

for k = 1:length(fieldnames(MachineModel))
    U = [U extractfield(MachineModel.(C{k}),'Error')'] ;
end
MeanM = mean(U,1)
%%

%% ------------ Functions
function R2 = rsquared(ActualVal,EstimateVal)
    if size(ActualVal) == size(EstimateVal) & isvector(ActualVal)
        RSS = sum((ActualVal - EstimateVal).^2) ;
        TSS = sum((ActualVal - mean(ActualVal)).^2) ;
        R2 = 1 - (RSS./TSS) ;
    else
        E = [] ;
    end
end

function E = rmse(ActualVal,EstimateVal)
    if size(ActualVal) == size(EstimateVal) & isvector(ActualVal)
        E = sqrt(sum(((ActualVal - EstimateVal).^2) ./ numel(ActualVal))) ;
    else
        E = [] ;
    end
end

function E = mae(ActualVal,EstimateVal)
    if size(ActualVal) == size(EstimateVal) & isvector(ActualVal)
        E = sum(abs(ActualVal-EstimateVal)) ./ numel(ActualVal) ;
    else
        E = [] ;
    end
end

function E = mape(ActualVal,EstimateVal)
    if size(ActualVal) == size(EstimateVal) & isvector(ActualVal)
        E = sum(abs(ActualVal-EstimateVal) ./ ActualVal) ./ numel(ActualVal) ;
    else
        E = [] ;
    end
end

function R = Reshaper(X)
    for i = 1:size(X,1)
        R(1,:,1,i) = X(i,:) ;
    end
end

function [xt , id] = MakeFeature(XT,MF)
    for i = 1:size(XT,2)
        id(:,i) = kmeans(squeeze(XT(:,i,:,:)),MF) ;
    end
    for i = 1:size(XT,4)
        ind = sub2ind([MF size(XT,2)],id(i,:),[1:size(XT,2)]) ;
        xtt = zeros(MF,size(XT,2)) ;
        xtt(ind) = XT(:,:,:,i) ;
        xt(:,:,1,i) = xtt ;
    end
end

function G = SameFinder(XT,YT,EFC)
    % EFC = ErrorFindingCategories
    for new = 1:size(YT)
        for i =1:size(XT,1)
            KO(i,new) = max(abs(([XT(new,:) YT(new,1)] - [XT(i,:) YT(i,1)]) ./ max([XT YT]))) ;
        end
    end
    GO = KO < EFC & KO ~= 0 ;
    G = graph(GO) ;
end

function [XToMachine , YToMachine , XT] = Duplicator(XT,YT,Q)
    xt = [] ;
    yt = [] ;
    for q = 1:Q
        xt = cat(4,xt,XT) ;
        yt = cat(1,yt,YT) ;
    end
    XToMachine = xt ;
    YToMachine = yt ;
end

function L = CNN_Connector(NameOfNet,Reserve,Dimension)
    switch lower(NameOfNet)
        case 'matlab'
            InputLayer = imageInputLayer(Dimension,'Name','Input') ;
            Reserve = layerGraph(Reserve) ;
            L = replaceLayer(Reserve,'Input',InputLayer);

        case 'alexnet'
            InputLayer = imageInputLayer(Dimension,'Name','Input') ;
            Reserve = layerGraph(Reserve) ;
            L = replaceLayer(Reserve,'Input',InputLayer);
    
        case 'darknet19'
            InputLayer = imageInputLayer(Dimension,'Name','Input') ;
            Reserve = layerGraph(Reserve) ;
            L = replaceLayer(Reserve,'Input',InputLayer);
    
        case 'lenet5'
            InputLayer = imageInputLayer(Dimension,'Name','Input') ;
            Reserve = layerGraph(Reserve) ;
            L = replaceLayer(Reserve,'Input',InputLayer);
    
        case 'mnist'
            InputLayer = imageInputLayer(Dimension,'Name','Input') ;
            Reserve = layerGraph(Reserve) ;
            L = replaceLayer(Reserve,'Input',InputLayer);
    
        case 'vgg16'
            InputLayer = imageInputLayer(Dimension,'Name','Input') ;
            Reserve = layerGraph(Reserve) ;
            L = replaceLayer(Reserve,'Input',InputLayer);
    
        case 'resnet18'
            InputLayer = imageInputLayer(Dimension,'Name','Input') ;
            L = replaceLayer(Reserve,'Input',InputLayer);
    
        case 'googlenet'
            InputLayer = imageInputLayer(Dimension,'Name','Input') ;
            L = replaceLayer(Reserve,'Input',InputLayer);
    end
end

function G = DataGraph(XTrain,YTrain,EFC)
    for new = 1:length(YTrain)
        for ii = 1:size(XTrain,1)
            KO(ii,new) = max(abs(([XTrain(new,:) YTrain(new,1)] - [XTrain(ii,:) YTrain(ii,1)]) ./ max([XTrain YTrain]))) ;
        end
    end
    GO = KO<EFC & KO~=0 ;
    G = graph(GO) ;
end

function id = DataConnect(XTrain,G,MF)
    id(:,size(XTrain,2)+1) = G.conncomp' ;
    for n = 1:size(XTrain,2)
        id(:,n) = kmeans(XTrain(:,n),MF) ;
    end
end

function R = ExpFunc(x,SDControl)
    switch SDControl
        case 'prod'
            R = 2*(0.001091*x^2 + 0.01348*x + 0.006988) ;
        case 'dive'  
            R = (0.001091*x^2 + 0.01348*x + 0.006988)/6 ;
    end           
end

function [GenFuzzy , n] = DataMF(GenFuzzy,XTrain,YTrain,MF,id,SDControl)
    for n = 1:size(XTrain,2)
        for m = 1:MF
            GenFuzzy.Inputs(1,n).MembershipFunctions(1,m).Type = 'gauss2mf' ;
            GenFuzzy.Inputs(1,n).MembershipFunctions(1,m).Parameters = [ExpFunc(mean(XTrain(:,n)),SDControl) min(XTrain(id(:,n)==m,n)) ExpFunc(mean(XTrain(:,n)),SDControl) max(XTrain(id(:,n)==m,n))] ;
        end
    end
    for m = 1:MF
        GenFuzzy.Outputs.MembershipFunctions(1,m).Type = 'gauss2mf' ;
        GenFuzzy.Outputs.MembershipFunctions(1,m).Parameters = [ExpFunc(mean(YTrain),SDControl) min(YTrain(id(:,n+1)==m)) ExpFunc(mean(YTrain),SDControl) max(YTrain(id(:,n+1)==m))] ;
    end
end

function GenFuzzy = DataRules(GenFuzzy,XTrain,YTrain,MF,id,n)
    for kk = 1:MF
        for nn = 1:size(XTrain,2)
            for m = 1:MF
                MeanClust(m,nn) = abs(mean([min(XTrain(id(:,n+1)==kk,nn)) max(XTrain(id(:,n+1)==kk,nn))] - [min(XTrain(id(:,nn)==m,nn)) max(XTrain(id(:,nn)==m,nn))])) ;
            end
        end
        R = [] ;
        for nn = 1:size(XTrain,2)
            [r , ~] = find(MeanClust(:,nn) == min(MeanClust(:,nn))) ; %%
            R = [R r(1)] ;
        end
        GenFuzzy.Rules(kk).Antecedent = R ;
        clear MeanClust 
        %     NR(kk,:) = R ;
    end
end

function [GenTree , NOMR] = GenFis(XTrain,YTrain,Struct,EFC,RepCol,SDControl)
    % EFC = ErrorFindingCategories
    switch Struct
        case 'Struct1'
            % Find related data
            G = DataGraph(XTrain,YTrain,EFC) ;
            % plot(G) ;
            MF = max(G.conncomp) ;

            Options = genfisOptions('FCMClustering','FISType','mamdani','Verbose',0,'NumClusters',MF,'Exponent',1.5) ;
            Options.MinImprovement = 1e-2 ; %Option for options>> Error for separete clusters
            Options.MaxNumIteration = 10 ;
            
            GenFuzzy = genfis(XTrain,YTrain,Options) ;
            
            % Link date
            id = DataConnect(XTrain,G,MF) ;

            % Make Membership functions
            [GenFuzzy , n] = DataMF(GenFuzzy,XTrain,YTrain,MF,id,SDControl) ;
  
            % Make rules
            GenFuzzy = DataRules(GenFuzzy,XTrain,YTrain,MF,id,n) ;
            
            % Con1
            Con1 = [] ;

            GenTree = fistree([GenFuzzy],Con1);
            NOMR = length(GenTree.FIS(end).Rules)+1 ;

        case 'Struct2'
            G = DataGraph(XTrain,YTrain,EFC) ;
            MF = max(G.conncomp) ;
            Options = genfisOptions('FCMClustering','FISType','mamdani','Verbose',0,'NumClusters',MF,'Exponent',1.5) ;
            Options.MinImprovement = 1e-2 ;
            Options.MaxNumIteration = 10 ;
            GenFuzzy1 = genfis(XTrain,YTrain,Options) ;
            id = DataConnect(XTrain,G,MF) ;
            [GenFuzzy1 , n] = DataMF(GenFuzzy1,XTrain,YTrain,MF,id,SDControl) ;
            GenFuzzy1 = DataRules(GenFuzzy1,XTrain,YTrain,MF,id,n) ;
            
            XTrain2 = [evalfis(GenFuzzy1,XTrain) XTrain(:,RepCol)] ;
            G = DataGraph(XTrain2,YTrain,EFC) ;
            MF = max(G.conncomp) ;
            Options.NumClusters = MF ;
            GenFuzzy2 = genfis(XTrain2,YTrain,Options) ;
            id = DataConnect(XTrain2,G,MF) ;
            [GenFuzzy2 , n] = DataMF(GenFuzzy2,XTrain2,YTrain,MF,id,SDControl) ;
            GenFuzzy2 = DataRules(GenFuzzy2,XTrain2,YTrain,MF,id,n) ;
            
            % Con1
            Con1 = [string([char(GenFuzzy1.Name) '/' char(GenFuzzy1.Outputs.Name)]) , string([char(GenFuzzy2.Name) '/' char(GenFuzzy2.Inputs(1).Name)])] ;
            
            % Con2
            Con2 = [string([char(GenFuzzy1.Name) '/' char(GenFuzzy1.Inputs(RepCol).Name)]) , string([char(GenFuzzy2.Name) '/' char(GenFuzzy2.Inputs(2).Name)])] ;
            
            GenTree = fistree([GenFuzzy1 GenFuzzy2],[Con1 ; Con2]);
            NOMR = max([length(GenTree.FIS(1).Rules) length(GenTree.FIS(2).Rules)])+1 ;

        case 'Struct3'
            G = DataGraph(XTrain,YTrain,EFC) ;
            MF = max(G.conncomp) ;
            Options = genfisOptions('FCMClustering','FISType','mamdani','Verbose',0,'NumClusters',MF,'Exponent',1.5) ;
            Options.MinImprovement = 1e-2 ;
            Options.MaxNumIteration = 10 ;
            GenFuzzy1 = genfis(XTrain,YTrain,Options) ;
            id = DataConnect(XTrain,G,MF) ;
            [GenFuzzy1 , n] = DataMF(GenFuzzy1,XTrain,YTrain,MF,id,SDControl) ;
            GenFuzzy1 = DataRules(GenFuzzy1,XTrain,YTrain,MF,id,n) ;
            
            XTrain2 = [XTrain(:,1:RepCol-1) XTrain(:,RepCol+1:end)] ;
            G = DataGraph(XTrain2,YTrain,EFC) ;
            MF = max(G.conncomp) ;
            Options.NumClusters = MF ;
            GenFuzzy2 = genfis(XTrain2,YTrain,Options) ;
            id = DataConnect(XTrain2,G,MF) ;
            [GenFuzzy2 , n] = DataMF(GenFuzzy2,XTrain2,YTrain,MF,id,SDControl) ;
            GenFuzzy2 = DataRules(GenFuzzy2,XTrain2,YTrain,MF,id,n) ;
            
            XTrain3 = [evalfis(GenFuzzy1,XTrain) evalfis(GenFuzzy2,XTrain2)] ;
            G = DataGraph(XTrain3,YTrain,EFC/2) ;
            MF = max(G.conncomp) ;
            Options.NumClusters = MF ;
            GenFuzzy3 = genfis(XTrain3,YTrain,Options) ;
            id = DataConnect(XTrain3,G,MF) ;
            [GenFuzzy3 , n] = DataMF(GenFuzzy3,XTrain3,YTrain,MF,id,SDControl) ;
            GenFuzzy3 = DataRules(GenFuzzy3,XTrain3,YTrain,MF,id,n) ;
            
            % Con1
            Con1 = [string([char(GenFuzzy1.Name) '/' char(GenFuzzy1.Outputs.Name)]) , string([char(GenFuzzy3.Name) '/' char(GenFuzzy3.Inputs(1).Name)]) ; string([char(GenFuzzy2.Name) '/' char(GenFuzzy2.Outputs.Name)]) , string([char(GenFuzzy3.Name) '/' char(GenFuzzy3.Inputs(2).Name)])] ;
            
            % Con2
            for i = 1:size(XTrain,2)-length(RepCol)
                Con2(i,:) = [string([char(GenFuzzy1.Name) '/' char(GenFuzzy1.Inputs(i).Name)]) , string([char(GenFuzzy2.Name) '/' char(GenFuzzy2.Inputs(i).Name)])] ;
                if i >= RepCol
                    Con2(i,:) = [string([char(GenFuzzy1.Name) '/' char(GenFuzzy1.Inputs(i+1).Name)]) , string([char(GenFuzzy2.Name) '/' char(GenFuzzy2.Inputs(i).Name)])] ;
                end
            end
    
            GenTree = fistree([GenFuzzy1 GenFuzzy2 GenFuzzy3],[Con1 ; Con2]);
            NOMR = length(GenTree.FIS(end).Rules)+1 ;

        case 'Struct4'
            G = DataGraph(XTrain,YTrain,EFC) ;
            MF = max(G.conncomp) ;
            Options = genfisOptions('FCMClustering','FISType','mamdani','Verbose',0,'NumClusters',MF,'Exponent',1.5) ;
            Options.MinImprovement = 1e-2 ;
            Options.MaxNumIteration = 10 ;
            GenFuzzy1 = genfis(XTrain,YTrain,Options) ;
            id = DataConnect(XTrain,G,MF) ;
            [GenFuzzy1 , n] = DataMF(GenFuzzy1,XTrain,YTrain,MF,id,SDControl) ;
            GenFuzzy1 = DataRules(GenFuzzy1,XTrain,YTrain,MF,id,n) ;

            XTrain2 = [XTrain(:,1:RepCol-1) XTrain(:,RepCol+1:end)] ;
            G = DataGraph(XTrain2,YTrain,EFC) ;
            MF = max(G.conncomp) ;
            Options.NumClusters = MF ;
            GenFuzzy2 = genfis(XTrain2,YTrain,Options) ;
            id = DataConnect(XTrain2,G,MF) ;
            [GenFuzzy2 , n] = DataMF(GenFuzzy2,XTrain2,YTrain,MF,id,SDControl) ;
            GenFuzzy2 = DataRules(GenFuzzy2,XTrain2,YTrain,MF,id,n) ;

            XTrain3 = [evalfis(GenFuzzy1,XTrain) evalfis(GenFuzzy2,XTrain2)] ;
            G = DataGraph(XTrain3,YTrain,EFC/2) ;
            MF = max(G.conncomp) ;
            Options.NumClusters = MF ;
            GenFuzzy3 = genfis(XTrain3,YTrain,Options) ;
            id = DataConnect(XTrain3,G,MF) ;
            [GenFuzzy3 , n] = DataMF(GenFuzzy3,XTrain3,YTrain,MF,id,SDControl) ;
            GenFuzzy3 = DataRules(GenFuzzy3,XTrain3,YTrain,MF,id,n) ;
            

            XTrain4 = [evalfis(GenFuzzy3,XTrain3) XTrain(:,RepCol)] ;
            G = DataGraph(XTrain4,YTrain,EFC/4) ;
            MF = max(G.conncomp) ;
            Options.NumClusters = MF ;
            GenFuzzy4 = genfis(XTrain4,YTrain,Options) ;
            id = DataConnect(XTrain4,G,MF) ;
            [GenFuzzy4 , n] = DataMF(GenFuzzy4,XTrain4,YTrain,MF,id,SDControl) ;
            GenFuzzy4 = DataRules(GenFuzzy4,XTrain4,YTrain,MF,id,n) ;
            GenFuzzy4.Name = "mamdani21-1" ;

            % Con1
            Con1 = [string([char(GenFuzzy1.Name) '/' char(GenFuzzy1.Outputs.Name)]) , string([char(GenFuzzy3.Name) '/' char(GenFuzzy3.Inputs(1).Name)]) ; string([char(GenFuzzy2.Name) '/' char(GenFuzzy2.Outputs.Name)]) , string([char(GenFuzzy3.Name) '/' char(GenFuzzy3.Inputs(2).Name)])] ;
            
            % Con2
            for i = 1:size(XTrain,2)-length(RepCol)
                Con2(i,:) = [string([char(GenFuzzy1.Name) '/' char(GenFuzzy1.Inputs(i).Name)]) , string([char(GenFuzzy2.Name) '/' char(GenFuzzy2.Inputs(i).Name)])] ;
                if i >= RepCol
                    Con2(i,:) = [string([char(GenFuzzy1.Name) '/' char(GenFuzzy1.Inputs(i+1).Name)]) , string([char(GenFuzzy2.Name) '/' char(GenFuzzy2.Inputs(i).Name)])] ;
                end
            end

            % Con3
            Con3 = [string([char(GenFuzzy3.Name) '/' char(GenFuzzy3.Outputs.Name)]) , string([char(GenFuzzy4.Name) '/' char(GenFuzzy4.Inputs(1).Name)]) ; string([char(GenFuzzy1.Name) '/' char(GenFuzzy1.Inputs(RepCol).Name)]) , string([char(GenFuzzy4.Name) '/' char(GenFuzzy4.Inputs(2).Name)])] ;
            
            GenTree = fistree([GenFuzzy1 GenFuzzy2 GenFuzzy3 GenFuzzy4],[Con1 ; Con2 ; Con3]);
            NOMR = length(GenTree.FIS(end).Rules)+1 ;
    end
end

function [G,MF,Dim,ID,XT,YT,DT,XTrain,YTrain,DTrain,XTest,YTest,DTest,xt,xtrain,xtest,indextrain,indextest] = PrepareData(XT,YT,DT,cvcoef,HyperParameter)
    G = SameFinder(XT,YT,HyperParameter) ; % XT is 2D
    MF = max(G.conncomp) ;
    xt = Reshaper(XT) ; % 4D
    Dim = [MF size(xt,2) 1] ;
    [xt , ID] = MakeFeature(xt,MF) ; % 4D
    
    % 70-30 Holdout validation
    CV = cvpartition(length(YT),'Holdout',cvcoef) ;
    indextrain = training(CV) ;
    indextest = test(CV) ;
    
    DTrain = DT(indextrain,:) ;
    DTest = DT(indextest,:) ;
    
    xtrain = xt(:,:,1,indextrain) ; % 4D
    xtest = xt(:,:,1,indextest) ; % 4D
    XTrain = XT(indextrain,:) ; % 2D
    XTest = XT(indextest,:) ; % 2D
    
    YTrain = YT(indextrain,:) ;
    YTest = YT(indextest,:) ;
end

function [Dim,ID,XT,YT,DT,XTrain,YTrain,DTrain,XTest,YTest,DTest,xt,xtrain,xtest] = PrepareDataBeforeCNN(XT,YT,DT,MF,indextrain,indextest) 
    XTrain = XT(indextrain,:) ; % 2D
    XTest = XT(indextest,:) ; % 2D
    
    xt = Reshaper(XT) ; % 4D
    Dim = [MF size(xt,2) 1] ;
    
    DTrain = DT(indextrain,:) ;
    DTest = DT(indextest,:) ;
    
    YTrain = YT(indextrain,:) ;
    YTest = YT(indextest,:) ;
    
    [xt,ID] = MakeFeature(xt,MF) ; % 4D % Changed ID size(NewXT)
    xtrain = xt(:,:,1,indextrain) ; % 4D
    xtest = xt(:,:,1,indextest) ; % 4D
end

function Net = CNNModel(XT,YT,XTest,YTest,Dim,WantStruct)
    % Initialize CNN
    % Iterations per epoch = Number of training samples ÷ MiniBatchSize
    % Max batch size= available GPU memory bytes / 4 / (size of tensors + trainable parameters)
    Iterations_Per_Epoch = 3 ;
    options = trainingOptions('sgdm', ... sgdm: stochastic gradient descent with momentum (SGDM) , rmsprop: rate of the squared gradient moving average , adam: Adam
        'MaxEpochs',100, ...
        'InitialLearnRate',1e-3, ...
        'LearnRateSchedule','piecewise', ...
        'LearnRateDropFactor',0.5, ...
        'LearnRateDropPeriod',10, ...
        'Shuffle','every-epoch', ...
        'ValidationFrequency',10, ...
        ...'Plots','training-progress', ...
        'Verbose',true);

    [XToMachine , YToMachine] = Duplicator(XT,YT,3) ; % integer and greater 1
    [XToVali , YToVali] = Duplicator(XTest,YTest,1) ; % integer and greater 1
    
    options.ValidationData = {XToVali,YToVali} ;
    options.MiniBatchSize  = ceil(numel(YT)/Iterations_Per_Epoch) ;
    options.OutputNetwork = 'best-validation-loss' ;
    
    load CNNStructures.mat
    eval(['Reserve = ' WantStruct ' ;'])
    layers = CNN_Connector(WantStruct,Reserve,Dim) ;
    
    Net = trainNetwork(XToMachine,YToMachine,layers,options) ;
end

function System = FuzzySystem(XT,YT,XTrain,YTrain,WantStruct,HyperParameter,ReplaceColumn,SDControl)
    Logic = max(XT) == min(XT) ;
    if any(Logic)
        Logic_Index = find(Logic == 1) ;
        for o = 1:sum(Logic)
            mu = 0 ; sigma = 0.00001 ;
            GD = gmdistribution(mu,sigma) ; Q = GD.random(length(YT))*sigma ;
            XT(:,Logic_Index(o)) = XT(:,Logic_Index(o)) + Q ;
            clear Q mu sigma o Logic Logic_Index
        end
    end
    Logic = max(XTrain) == min(XTrain) ;
    if any(Logic)
        Logic_Index = find(Logic == 1) ;
        for o = 1:sum(Logic)
            mu = 0 ; sigma = 0.00001 ;
            GD = gmdistribution(mu,sigma) ; Q = GD.random(length(YTrain))*sigma ;
            XTrain(:,Logic_Index(o)) = XTrain(:,Logic_Index(o)) + Q ;
            clear Q mu sigma o Logic Logic_Index
        end
    end

    % Initialize Fuzzy 
    OptionsTune = tunefisOptions ;
    OptionsTune.Method = 'particleswarm' ;
    OptionsTune.OptimizationType = 'learning' ;
    OptionsTune.Display = 'none' ;
    OptionsTune.UseParallel = false ; 
    OptionsTune.MethodOptions.MaxIterations = 5 ;
    
    while true
        try
            disp('work on it.')
            [Tree , NumOfRulesForTune] = GenFis(XT,YT,WantStruct,HyperParameter,ReplaceColumn,SDControl) ; % "ReplaceColumn" only use in Structure 2,3,4
            disp('done.')
            break
        catch
        end
    end

    OptionsTune.NumMaxRules = NumOfRulesForTune ;
    System = tunefis(Tree,[],XTrain,YTrain,OptionsTune) ; % Tuned
end


function Net = ANN_Network(ANN)
    switch lower(ANN)
        case 'feedforward'
            InputConnection = [1;...
                0 ;...
                0 ;...
                0 ;...
                0 ;...
                ];
            LayerConnection = [0 0 0 0 0 ;...
                1 0 0 0 0 ;...
                0 1 0 0 0 ;...
                0 0 1 0 0 ;...
                0 0 0 1 0 ;...
                ];
            OutputConnection = [0 0 0 0 1] ;
            BiasConnection = [1 ; 0 ; 1 ; 0 ; 1] ;
            Net = network(1,5) ;
            Net = init(Net) ;
            
            Net.biasConnect = BiasConnection ;
            Net.inputConnect = InputConnection ;
            Net.layerConnect = LayerConnection ;
            Net.outputConnect = OutputConnection ;
            
            Net.trainFcn = 'trainlm' ;
            
            Net.layers{1}.transferFcn = 'tansig';
            Net.layers{2}.transferFcn = 'tansig';
            Net.layers{3}.transferFcn = 'tansig';
            Net.layers{4}.transferFcn = 'tansig';
            Net.layers{5}.transferFcn = 'purelin';
            
            Net.layers{1}.size = 20 ;
            Net.layers{2}.size = 15 ;
            Net.layers{3}.size = 10 ;
            Net.layers{4}.size = 5 ;
            Net.layers{5}.size = 1 ;
            
            Net.layers{1}.initFcn = 'initnw' ;
            Net.layers{2}.initFcn = 'initnw' ;
            Net.layers{3}.initFcn = 'initnw' ;
            Net.layers{4}.initFcn = 'initnw' ;
            Net.layers{5}.initFcn = 'initnw' ;

        case 'cascade'
            InputConnection = [1;...
                1 ;...
                0 ;...
                1 ;...
                0 ;...
                ];
            LayerConnection = [0 0 0 0 0 ;...
                1 0 0 0 0 ;...
                0 1 0 0 0 ;...
                0 0 1 0 0 ;...
                0 0 0 1 0 ;...
                ];
            OutputConnection = [0 0 0 0 1] ;
            Net = network(1,5,[1 ; 0 ; 1 ; 0 ; 1],InputConnection,LayerConnection,OutputConnection) ;
            % init(net) ;
            % https://www.mathworks.com/help/deeplearning/ug/choose-a-multilayer-neural-network-training-function.html
            Net.trainFcn = 'trainlm' ;
            
            
            % http://matlab.izmiran.ru/help/toolbox/nnet/tabls149.html
            Net.layers{1}.transferFcn = 'logsig';
            Net.layers{2}.transferFcn = 'logsig';
            Net.layers{3}.transferFcn = 'logsig';
            Net.layers{4}.transferFcn = 'logsig'; % tribas
            Net.layers{5}.transferFcn = 'purelin'; % satlins
            
            
            Net.layers{1}.size = 20 ;
            Net.layers{2}.size = 15 ;
            Net.layers{3}.size = 10 ;
            Net.layers{4}.size = 5 ;
            Net.layers{5}.size = 1 ;
            
            % This is important
            Net.layers{1}.initFcn = 'initnw'; % 'initnw' 'initlay' 'initwb' 'rands' 'revert'
            Net.layers{2}.initFcn = 'initnw';
            Net.layers{3}.initFcn = 'initnw';
            Net.layers{4}.initFcn = 'initnw';
            Net.layers{5}.initFcn = 'initnw';
        case 'perceptron'
            InputConnection = [1;...
                0 ;...
                0 ;...
                0 ;...
                0 ;...
                ];
            LayerConnection = [0 0 0 0 0 ;...
                1 0 0 0 0 ;...
                0 1 0 0 0 ;...
                0 0 1 0 0 ;...
                0 0 0 1 0 ;...
                ];
            OutputConnection = [0 0 0 0 1] ;
            Net = network(1,5,[1 ; 1 ; 1 ; 1 ; 1],InputConnection,LayerConnection,OutputConnection) ;
            % init(net) ;
            % https://www.mathworks.com/help/deeplearning/ug/choose-a-multilayer-neural-network-training-function.html
            Net.trainFcn = 'trainlm' ;
            
            
            % http://matlab.izmiran.ru/help/toolbox/nnet/tabls149.html
            Net.layers{1}.transferFcn = 'hardlim';
            Net.layers{2}.transferFcn = 'hardlim';
            Net.layers{3}.transferFcn = 'hardlim';
            Net.layers{4}.transferFcn = 'hardlim';
            Net.layers{5}.transferFcn = 'purelin';
            
            
            Net.layers{1}.size = 20 ;
            Net.layers{2}.size = 15 ;
            Net.layers{3}.size = 10 ;
            Net.layers{4}.size = 5 ;
            Net.layers{5}.size = 1 ;
            
            % This is important
            Net.layers{1}.initFcn = 'initnw'; %'initnw' 'initlay' 'initwb' 'rands' 'revert'
            Net.layers{2}.initFcn = 'initnw' ;
            Net.layers{3}.initFcn = 'initnw' ;
            Net.layers{4}.initFcn = 'initnw' ;
            Net.layers{5}.initFcn = 'initnw' ;
        case {'rbf','radialbasisfunction'}
            InputConnection = [1;...
                0 ;...
                0 ;...
                0 ;...
                0 ;...
                ];
            LayerConnection = [0 0 0 0 0 ;...
                1 0 0 0 0 ;...
                0 1 0 0 0 ;...
                0 0 1 0 0 ;...
                0 0 0 1 0 ;...
                ];
            OutputConnection = [0 0 0 0 1] ;
            Net = network(1,5,[1 ; 1 ; 1 ; 1 ; 1],InputConnection,LayerConnection,OutputConnection) ;
            % init(net) ;
            % https://www.mathworks.com/help/deeplearning/ug/choose-a-multilayer-neural-network-training-function.html
            Net.trainFcn = 'trainlm' ;
            
            
            % http://matlab.izmiran.ru/help/toolbox/nnet/tabls149.html
            Net.layers{1}.transferFcn = 'radbas';
            Net.layers{2}.transferFcn = 'radbas';
            Net.layers{3}.transferFcn = 'radbas';
            Net.layers{4}.transferFcn = 'radbas'; % tribas
            Net.layers{5}.transferFcn = 'purelin'; % satlins
            
            
            Net.layers{1}.size = 20 ;
            Net.layers{2}.size = 15 ;
            Net.layers{3}.size = 10 ;
            Net.layers{4}.size = 5 ;
            Net.layers{5}.size = 1 ;
            
            % This is important
            Net.layers{1}.initFcn = 'initnw'; % 'initnw' 'initlay' 'initwb' 'rands' 'revert'
            Net.layers{2}.initFcn = 'initnw';
            Net.layers{3}.initFcn = 'initnw';
            Net.layers{4}.initFcn = 'initnw';
            Net.layers{5}.initFcn = 'initnw';
            
            Net.layers{1}.netInputFcn = 'netprod' ;
            Net.layers{2}.netInputFcn = 'netprod' ;
            Net.layers{3}.netInputFcn = 'netprod' ;
            Net.layers{4}.netInputFcn = 'netprod' ;
            Net.layers{5}.netInputFcn = 'netsum' ;
        case {'rnn' , 'recurrentneuralnetwork'}
            InputConnection = [1;...
                0 ;...
                0 ;...
                0 ;...
                0 ;...
                ];
            LayerConnection = [0 0 0 0 0 ;...
                1 1 0 0 0 ;...
                0 1 0 0 0 ;...
                0 0 1 1 0 ;...
                0 0 0 1 0 ;...
                ];
            OutputConnection = [0 0 0 0 1] ;
            Net = network(1,5,[1 ; 0 ; 1 ; 0 ; 1],InputConnection,LayerConnection,OutputConnection) ;
            init(Net) ;
            % https://www.mathworks.com/help/deeplearning/ug/choose-a-multilayer-neural-network-training-function.html
            Net.trainFcn = 'trainlm' ;
            
            
            % http://matlab.izmiran.ru/help/toolbox/nnet/tabls149.html
            Net.layers{1}.transferFcn = 'logsig';
            Net.layers{2}.transferFcn = 'logsig';
            Net.layers{3}.transferFcn = 'logsig';
            Net.layers{4}.transferFcn = 'logsig'; % tribas
            Net.layers{5}.transferFcn = 'purelin'; % satlins
            
            
            Net.layers{1}.size = 20 ;
            Net.layers{2}.size = 15 ;
            Net.layers{3}.size = 10 ;
            Net.layers{4}.size = 5 ;
            Net.layers{5}.size = 1 ;
            
            % delay for rnn
            Net.layerWeights{2,2}.delays = [1];
            Net.layerWeights{4,4}.delays = [1];

            % This is important
            Net.layers{1}.initFcn = 'initnw'; % 'initnw' 'initlay' 'initwb' 'rands' 'revert'
            Net.layers{2}.initFcn = 'initnw';
            Net.layers{3}.initFcn = 'initnw';
            Net.layers{4}.initFcn = 'initnw';
            Net.layers{5}.initFcn = 'initnw';
    end
    
end
function [ExtraMachine , AddStruct] = AddMachineSelector(XT,YT,WantStruct,Method,CrossVal)
    switch Method
        case 1
            Model = fitrsvm(XT,YT,'Standardize',true,'KernelFunction',WantStruct,'Holdout',CrossVal) ;
            ExtraMachine = Model.Trained{1} ;
            AddStruct = 'SVM' ;
        case 2
            net = ANN_Network(WantStruct) ;
            net.trainParam.epochs = 100 ;
            net.trainParam.goal = 1e-7;
            net.trainParam.max_fail = 100 ;
            net.trainParam.time = 100 ; % sec
            net.trainParam.showWindow = false;
    
            % Train
            net = train(net,XT',YT') ;
            ExtraMachine = net ;
            AddStruct = 'ANN' ;
    end
    disp(AddStruct)
end