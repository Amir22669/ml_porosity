function Data = Data2DtoDataCNN(Samples,ID,XT,MF)
    Data = zeros(MF,size(Samples,2)) ;
    for j = 1:size(Samples,1)
        sample = Samples(j,:) ;
        for i = 1:size(sample,2)
            ido = find(min(abs(sample(:,i) - XT(:,i))) == abs(sample(:,i) - XT(:,i))) ;
            Data(ID(ido(1),i),i,1,j) = sample(:,i) ;
        end
    end
end