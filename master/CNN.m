clear all ; close all ; clc %% CNN %%
warning off
%%
load DataFile.mat
WellNames = DataStruct.WellNames ;
NumberOfHoles = DataStruct.NumberOfHoles ;
%% https://www.mathworks.com/help/deeplearning/ug/list-of-deep-learning-layers.html#mw_ba2587f2-c877-4d5c-b500-92762cb4af82
Structural = {'MATLAB' , 'MNIST' , 'LeNet5' , 'AlexNet' , 'DarkNet19' , 'ResNet18' } ; % 'VGG16' , 'GoogleNet'
%% Close(false), Open(true) figures
FigureFlag = false ;
%% Iterations per epoch = Number of training samples ÷ MiniBatchSize
% Max batch size= available GPU memory bytes / 4 / (size of tensors + trainable parameters)
Iterations_Per_Epoch = 3 ;
options = trainingOptions('sgdm', ... sgdm: stochastic gradient descent with momentum (SGDM) , rmsprop: rate of the squared gradient moving average , adam: Adam
    'MaxEpochs',100, ...
    'InitialLearnRate',1e-3, ...
    'LearnRateSchedule','piecewise', ...
    'LearnRateDropFactor',0.5, ...
    'LearnRateDropPeriod',10, ...
    'Shuffle','every-epoch', ...
    'ValidationFrequency',10, ...
    ...'Plots','training-progress', ...
    'Verbose',false);

%%
for i = 6%:length(WellNames)

    % from Core properties and porosity
    DataStruct.(WellNames{i}).Core_Properties.PHIE = DataStruct.(WellNames{i}).Core_Porosity ;
    try
        DataStruct.(WellNames{i}).Core_Properties.DT = [] ; % Remove DT
    catch
    end
    DataStruct.(WellNames{i}).Core_Properties = rmmissing(DataStruct.(WellNames{i}).Core_Properties) ; % Remove NaN Rows

    N = table2array(DataStruct.(WellNames{i}).Core_Properties) ;
    N(N==0) = 1e-4 ;
    DataStruct.(WellNames{i}).Core_Properties = array2table(N,"VariableNames",DataStruct.(WellNames{i}).Core_Properties.Properties.VariableNames) ;
    clear N

    DT = DataStruct.(WellNames{i}).Core_Properties.DEPTH ;
    YT = 100 * DataStruct.(WellNames{i}).Core_Properties.PHIE ; % fraction to percent
    XT = [DataStruct.(WellNames{i}).Core_Properties.BS DataStruct.(WellNames{i}).Core_Properties.CALI DataStruct.(WellNames{i}).Core_Properties.GR DataStruct.(WellNames{i}).Core_Properties.NPHI DataStruct.(WellNames{i}).Core_Properties.RHOB DataStruct.(WellNames{i}).Core_Properties.DRHO] ;

    Sec_Range = [min(DT) max(DT)] ;

    Logic = max(XT) == min(XT) ;
    if any(Logic)
        Logic_Index = find(Logic == 1) ;
        for o = 1:sum(Logic)
            mu = 0 ; sigma = 0.00001 ;
            GD = gmdistribution(mu,sigma) ; Q = GD.random(length(YT))*sigma ;
            XT(:,Logic_Index(o)) = XT(:,Logic_Index(o)) + Q ;
            clear Q mu sigma o
        end
    end

    G = SameFinder(XT,YT,0.01) ;
    % figure ; plot(G) ;
    MF = max(G.conncomp) ;

    Dim = [MF size(XT,2) 1] ;

    XT = Reshaper(XT) ;
    XT = MakeFeature(XT,MF) ;

    % 70-30 Holdout validation
    CV = cvpartition(length(YT),'Holdout',0.3) ;
    indextrain = training(CV) ;
    indextest = test(CV) ;

    DTrain = DT(indextrain,:) ;
    DTest = DT(indextest,:) ;

    XTrain = XT(:,:,1,indextrain) ;
    XTest = XT(:,:,1,indextest) ;

    YTrain = YT(indextrain,:) ;
    YTest = YT(indextest,:) ;

    [XToMachine , YToMachine] = Duplicator(XT,YT,2) ; % integer and greater 1
    [XToVali , YToVali] = Duplicator(XTest,YTest,1) ; % integer and greater 1




    % Train
    options.ValidationData = {XToVali,YToVali} ;
    options.MiniBatchSize  = 20 ; %ceil(numel(YTrain)/(1*Iterations_Per_Epoch)) ;
    options.OutputNetwork = 'best-validation-loss' ;

    for k = 1%:length(Structural)
        WantStruct = Structural{k} ;
        layers = CNN_Selector(WantStruct,Dim) ;

        net = trainNetwork(XToMachine,YToMachine,layers,options) ;

        if FigureFlag
            figure('Color','w','Name',[WellNames{i} ' , ' WantStruct] , 'NumberTitle','off')
        end

        for j = 0:NumberOfHoles(i)
            if FigureFlag
                Ax(j+1) = subplot(1,NumberOfHoles(i)+2,j+1) ; Ax(j+1).XColor = 'none' ; Ax(j+1).YColor = 'none' ;
                try
                    DataStruct.(WellNames{i}).(['Log_' num2str(j)]).DT = [] ; % Remove DT
                catch
                end
                DataStruct.(WellNames{i}).(['Log_' num2str(j)]) = rmmissing(DataStruct.(WellNames{i}).(['Log_' num2str(j)])) ;
                if ~isempty(DataStruct.(WellNames{i}).(['Log_' num2str(j)])) & size(DataStruct.(WellNames{i}).(['Log_' num2str(j)]),1) >= MF
                    Ax(j+1).XColor = 'black' ; Ax(j+1).YColor = 'black' ;

                    XL = [DataStruct.(WellNames{i}).(['Log_' num2str(j)]).BS DataStruct.(WellNames{i}).(['Log_' num2str(j)]).CALI DataStruct.(WellNames{i}).(['Log_' num2str(j)]).GR DataStruct.(WellNames{i}).(['Log_' num2str(j)]).NPHI DataStruct.(WellNames{i}).(['Log_' num2str(j)]).RHOB DataStruct.(WellNames{i}).(['Log_' num2str(j)]).DRHO] ;
                    YL = 100 * DataStruct.(WellNames{i}).(['Log_' num2str(j)]).PHIE ;
                    DL = DataStruct.(WellNames{i}).(['Log_' num2str(j)]).DEPTH ;
                    XL = Reshaper(XL) ;
                    XL = MakeFeature(XL,MF) ;

                    h1 = plot(Ax(j+1),YL/100,DL,'g','LineWidth',2) ; Ax(j+1).YDir = 'reverse' ; hold(Ax(j+1),'on') ;
                    h2 = plot(Ax(j+1),predict(net,XL)/100,DL,'b','LineWidth',2) ;
                    h3 = plot(Ax(j+1),YT/100,DT,'Marker','o','MarkerFaceColor','g','MarkerSize',7,'MarkerEdgeColor','k','LineStyle','none') ;
                    h4 = scatter(Ax(j+1),predict(net,XTrain)/100,DTrain,23,'Marker','o','MarkerFaceColor',[0.1 0.1 0.9],'MarkerEdgeColor',[0.9370 0.9680 0.0190],'MarkerFaceAlpha',0.8) ;
                    h5 = scatter(Ax(j+1),predict(net,XTest)/100,DTest,23,'Marker','o','MarkerFaceColor',[0.9 0.1 0.1],'MarkerEdgeColor',[0.9290 0.6940 0.1250],'MarkerFaceAlpha',0.8) ;
                    %
                    Err = mape(YL/100,double(predict(net,XL)/100))*100 ;
                    Err(Err>100) = 100 ;
                    %
                    title(Ax(j+1),['Log Error_{MAPE}: ' num2str(Err,'%2.2f') '%'],'FontSize',7)
                    Ax(j+1).XAxis.Limits(2) = 0.4 ; Ax(j+1).XAxis.Limits(1) = 0 ;
                    Ax(j+1).YAxis.Limits(2) = Sec_Range(2)+1 ; Ax(j+1).YAxis.Limits(1) = Sec_Range(1)-1 ;
                    xlabel(Ax(j+1),'Porosity(Fraction)') ;
                end
            end
        end
        if FigureFlag
            Ax(j+2) = subplot(1,NumberOfHoles(i)+2,NumberOfHoles(i)+2) ; hold(Ax(j+2),'on') ; box(Ax(j+2),'on') ;
            plot(Ax(j+2),YT/100,DT,'Marker','o','MarkerFaceColor','g','MarkerSize',7,'MarkerEdgeColor','k','LineStyle','none') ;
            scatter(Ax(j+2),predict(net,XTrain)/100,DTrain,23,'Marker','o','MarkerFaceColor',[0.1 0.1 0.9],'MarkerEdgeColor',[0.9370 0.9680 0.0190],'MarkerFaceAlpha',0.8) ;
            scatter(Ax(j+2),predict(net,XTest)/100,DTest,23,'Marker','o','MarkerFaceColor',[0.9 0.1 0.1],'MarkerEdgeColor',[0.9290 0.6940 0.1250],'MarkerFaceAlpha',0.8) ;
    
            Err = mape(YT/100,double(predict(net,XT)/100))*100 ; % 0 is good, 100 is bad.
            Err(Err>100) = 100 ;
    
    
            Ax(j+2).XAxis.Limits(2) = 0.4 ; Ax(j+2).XAxis.Limits(1) = 0 ;
            Ax(j+2).YAxis.Limits(2) = Sec_Range(2)+1 ; Ax(j+2).YAxis.Limits(1) = Sec_Range(1)-1 ;
            title(Ax(j+2),['Core Error_{MAPE}: ' num2str(Err,'%2.2f') '%'],'FontSize',7) ; Ax(j+2).YDir = 'reverse' ;
            Ax(j+2).YAxisLocation = 'right' ;
            ylabel(Ax(j+2),'Depth(m)') ; xlabel(Ax(j+2),'Porosity(Fraction)') ;
    
            try
                legend(Ax(j+1),[h1 , h2 , h3 , h4 , h5] , {'Log Data' , 'Predict Log' , 'Core Data' , 'Predict Core Train' , 'Predict Core Test'})
            catch
            end
            % Validation
            figure('Color','w','Name',[WellNames{i} ' , ' WantStruct ' , Cross Validation'] , 'NumberTitle','off')
            subplot(2,2,1) ; plot(predict(net,XTrain)/100,YTrain/100,'Marker','o','MarkerFaceColor',[0.1 0.1 0.9],'LineStyle','none','MarkerEdgeColor','none') ; hold on ; plot([0 0.5] , [0 0.5] , 'k' , 'LineWidth' , 2) ; ylabel("Actual Porosity with Training Data") ; xlabel("Predict Porosity with Training Data")
            title(['Training Data R^{2}: ' num2str(rsquared(YTrain,predict(net,XTrain)),'%2.2f')])
            axis([0 0.5 0 0.5])
    
            subplot(2,2,2) ; plot(predict(net,XTest)/100,YTest/100,'Marker','o','MarkerFaceColor',[0.9 0.1 0.1],'LineStyle','none','MarkerEdgeColor','none') ; hold on ; plot([0 0.5] , [0 0.5] , 'k' , 'LineWidth' , 2) ; ylabel("Actual Porosity with Testing Data") ; xlabel("Predict Porosity with Testing Data")
            title(['Testing Data R^{2}: ' num2str(rsquared(YTest,predict(net,XTest)),'%2.2f')])
            axis([0 0.5 0 0.5])
    
            subplot(2,2,3) ; plot(predict(net,XT)/100,YT/100,'Marker','o','MarkerFaceColor','g','LineStyle','none','MarkerEdgeColor','none') ; hold on ; plot([0 0.5] , [0 0.5] , 'k' , 'LineWidth' , 2) ; ylabel("Actual Porosity with All Data") ; xlabel("Predict Porosity with All Data")
            title(['All Data R^{2}: ' num2str(rsquared(YT,predict(net,XT)),'%2.2f')])
            axis([0 0.5 0 0.5])
        end
        CNNModel.(WantStruct)(i).CNN = net ;
        CNNModel.(WantStruct)(i).Error = double(rsquared(YTest,predict(net,XTest))) ;
        disp([num2str(i) ' , ' WantStruct])
       
    end
end

%% Mean error of structures
M = [] ;
C = fieldnames(CNNModel) ;

for k = 1:length(fieldnames(CNNModel))
    M = [M extractfield(CNNModel.(C{k}),'Error')'] ;
end
MeanM = mean(M,1)
%%
% for k = 1:length(fieldnames(CNNModel))
%     analyzeNetwork(CNNModel.(C{k})(i).CNN)
% end

%% ------------ Functions
function R2 = rsquared(ActualVal,EstimateVal)
if size(ActualVal) == size(EstimateVal) & isvector(ActualVal)
    RSS = sum((ActualVal - EstimateVal).^2) ;
    TSS = sum((ActualVal - mean(ActualVal)).^2) ;
    R2 = 1 - (RSS./TSS) ;
else
    E = [] ;
end
end

function E = rmse(ActualVal,EstimateVal)
if size(ActualVal) == size(EstimateVal) & isvector(ActualVal)
    E = sqrt(sum(((ActualVal - EstimateVal).^2) ./ numel(ActualVal))) ;
else
    E = [] ;
end
end


function E = mae(ActualVal,EstimateVal)
if size(ActualVal) == size(EstimateVal) & isvector(ActualVal)
    E = sum(abs(ActualVal-EstimateVal)) ./ numel(ActualVal) ;
else
    E = [] ;
end
end


function E = mape(ActualVal,EstimateVal)
if size(ActualVal) == size(EstimateVal) & isvector(ActualVal)
    E = sum(abs(ActualVal-EstimateVal) ./ ActualVal) ./ numel(ActualVal) ;
else
    E = [] ;
end
end


function R = Reshaper(X)
for i = 1:size(X,1)
    R(1,:,1,i) = X(i,:) ;
end
end

function xt = MakeFeature(XT,MF)
for i = 1:size(XT,2)
    id(:,i) = kmeans(squeeze(XT(:,i,:,:)),MF) ;
end
for i = 1:size(XT,4)
    ind = sub2ind([MF size(XT,2)],id(i,:),[1:size(XT,2)]) ;
    xtt = zeros(MF,size(XT,2)) ;
    xtt(ind) = XT(:,:,:,i) ;
    xt(:,:,1,i) = xtt ;
end
end

function G = SameFinder(XT,YT,EFC)
% EFC = ErrorFindingCategories
for new = 1:size(YT)
    for i =1:size(XT,1)
        KO(i,new) = max(abs(([XT(new,:) YT(new,1)] - [XT(i,:) YT(i,1)]) ./ max([XT YT]))) ;
    end
end
GO = KO < EFC & KO ~= 0 ;
G = graph(GO) ;
end


function [XToMachine , YToMachine , XT] = Duplicator(XT,YT,Q)
xt = [] ;
yt = [] ;
for q = 1:Q
    xt = cat(4,xt,XT) ;
    yt = cat(1,yt,YT) ;
end

XToMachine = xt ;
YToMachine = yt ;

end


function L = CNN_Selector(NameOfNet,Dimension)
switch lower(NameOfNet)
    case 'matlab'
        L = [imageInputLayer(Dimension)
            convolution2dLayer(3,8,'Padding','same')
            batchNormalizationLayer
            %reluLayer
            averagePooling2dLayer(2,'Stride',2)
            convolution2dLayer(3,16,'Padding','same')
            batchNormalizationLayer
            reluLayer
            averagePooling2dLayer(2,'Stride',2)
            convolution2dLayer(3,32,'Padding','same')
            batchNormalizationLayer
            reluLayer
            convolution2dLayer(3,32,'Padding','same')
            batchNormalizationLayer
            reluLayer
            fullyConnectedLayer(1)
            regressionLayer];
    case 'alexnet'
        L = [imageInputLayer(Dimension)
            convolution2dLayer([size(Dimension,1) size(Dimension,2)],96,'Padding','same','Stride',[1 6])
            maxPooling2dLayer(1,'Padding','same','Stride',1)
            convolution2dLayer(1,256,'Padding','same','Stride',1)
            maxPooling2dLayer(1,'Padding','same','Stride',1)
            convolution2dLayer(1,384,'Padding','same','Stride',1)
            convolution2dLayer(1,384,'Padding','same','Stride',1)
            convolution2dLayer(1,256,'Padding','same','Stride',1)
            maxPooling2dLayer(1,'Padding','same','Stride',1)
            fullyConnectedLayer(2048)
            fullyConnectedLayer(2048)
            fullyConnectedLayer(1)
            ...softmaxLayer
            regressionLayer];
    case 'darknet19'
        L = [imageInputLayer(Dimension)
            convolution2dLayer([size(Dimension,1) size(Dimension,2)],32,"Padding","same",'Stride',[1 6])
            batchNormalizationLayer
            maxPooling2dLayer(1,"Stride",1,"Padding","same")
            convolution2dLayer(1,64,"Padding","same",'Stride',1)
            batchNormalizationLayer
            maxPooling2dLayer(1,"Stride",1,"Padding","same")
            convolution2dLayer(1,128,"Padding","same",'Stride',1)
            batchNormalizationLayer
            convolution2dLayer(1,64,"Padding","same",'Stride',1)
            batchNormalizationLayer
            convolution2dLayer(1,128,"Padding","same",'Stride',1)
            batchNormalizationLayer
            maxPooling2dLayer(1,"Stride",1,"Padding","same")
            convolution2dLayer(1,256,"Padding","same",'Stride',1)
            batchNormalizationLayer
            convolution2dLayer(1,128,"Padding","same",'Stride',1)
            batchNormalizationLayer
            convolution2dLayer(1,256,"Padding","same",'Stride',1)
            batchNormalizationLayer
            maxPooling2dLayer(1,"Stride",1,"Padding","same")
            convolution2dLayer(1,512,"Padding","same",'Stride',1)
            batchNormalizationLayer
            convolution2dLayer(1,256,"Padding","same",'Stride',1)
            batchNormalizationLayer
            convolution2dLayer(1,512,"Padding","same",'Stride',1)
            batchNormalizationLayer
            convolution2dLayer(1,256,"Padding","same",'Stride',1)
            batchNormalizationLayer
            convolution2dLayer(1,512,"Padding","same",'Stride',1)
            batchNormalizationLayer
            maxPooling2dLayer(1,"Stride",1,"Padding","same")
            convolution2dLayer(1,1024,"Padding","same",'Stride',1)
            batchNormalizationLayer
            convolution2dLayer(1,512,"Padding","same",'Stride',1)
            batchNormalizationLayer
            convolution2dLayer(1,1024,"Padding","same",'Stride',1)
            batchNormalizationLayer
            convolution2dLayer(1,512,"Padding","same",'Stride',1)
            batchNormalizationLayer
            convolution2dLayer(1,1024,"Padding","same",'Stride',1)
            batchNormalizationLayer
            convolution2dLayer(1,1,"Padding","same",'Stride',1)
            globalAveragePooling2dLayer
            ...softmaxLayer
            regressionLayer];
    case 'lenet5'
        L = [imageInputLayer(Dimension)
            convolution2dLayer([size(Dimension,1) size(Dimension,2)],32,"Padding","same","Stride",[size(Dimension,1) size(Dimension,2)])
            averagePooling2dLayer(2,"Padding","same","Stride",1)
            convolution2dLayer(5,16,"Padding","same","Stride",1)
            averagePooling2dLayer(2,"Padding","same","Stride",1)
            convolution2dLayer(5,120,"Padding","same","Stride",1)
            fullyConnectedLayer(84)
            fullyConnectedLayer(1)
            ...softmaxLayer
            regressionLayer];
    case 'mnist'
        L = [imageInputLayer(Dimension)
            convolution2dLayer([size(Dimension,1) size(Dimension,2)],32,"Padding","same","Stride",[size(Dimension,1) size(Dimension,2)])
            reluLayer
            maxPooling2dLayer(1,"Padding","same","Stride",2)
            convolution2dLayer(3,64,"Padding","same","Stride",1)
            reluLayer
            maxPooling2dLayer(1,"Padding","same","Stride",2)
            fullyConnectedLayer(3136) % flatten - 3136 for this case; In fact, the number of elements is in the previous layer
            fullyConnectedLayer(128)
            fullyConnectedLayer(1)
            regressionLayer];
    case 'vgg16'
        L = [imageInputLayer(Dimension)
            convolution2dLayer([size(Dimension,1) size(Dimension,2)],64,"Padding",1,"Stride",[size(Dimension,1) size(Dimension,2)])
            reluLayer
            convolution2dLayer(1,64,"Padding",'same',"Stride",1)
            reluLayer
            maxPooling2dLayer(1,"Padding",0,"Stride",1)
            convolution2dLayer(1,128,"Padding",'same',"Stride",1)
            reluLayer
            convolution2dLayer(1,128,"Padding",'same',"Stride",1)
            reluLayer
            maxPooling2dLayer(1,"Padding",0,"Stride",1)
            convolution2dLayer(1,256,"Padding",'same',"Stride",1)
            reluLayer
            convolution2dLayer(1,256,"Padding",'same',"Stride",1)
            reluLayer
            convolution2dLayer(1,256,"Padding",'same',"Stride",1)
            reluLayer
            maxPooling2dLayer(1,"Padding",0,"Stride",1)
            convolution2dLayer(1,512,"Padding",'same',"Stride",1)
            reluLayer
            convolution2dLayer(1,512,"Padding",'same',"Stride",1)
            reluLayer
            convolution2dLayer(1,512,"Padding",'same',"Stride",1)
            reluLayer
            maxPooling2dLayer(1,"Padding",0,"Stride",1)
            convolution2dLayer(1,512,"Padding",'same',"Stride",1)
            reluLayer
            convolution2dLayer(1,512,"Padding",'same',"Stride",1)
            reluLayer
            convolution2dLayer(1,512,"Padding",'same',"Stride",1)
            reluLayer
            maxPooling2dLayer(1,"Padding",0,"Stride",1)
            fullyConnectedLayer(1016) % 4096 -- Hang
            reluLayer
            fullyConnectedLayer(512) % 4096 -- Hang
            reluLayer
            fullyConnectedLayer(1)
            reluLayer
            ...softmaxLayer
            regressionLayer];
    case 'resnet18'
        lgraph = layerGraph();
        tempLayers = [imageInputLayer([Dimension],"Name","data")
            convolution2dLayer([size(Dimension,1) size(Dimension,2)],64,"Name","conv1","Padding",3,"Stride",[size(Dimension,1) size(Dimension,2)])
            batchNormalizationLayer("Name","bn_conv1")
            reluLayer("Name","conv1_relu")
            maxPooling2dLayer(2,"Name","pool1","Padding",1,"Stride",2)];
        lgraph = addLayers(lgraph,tempLayers);


        tempLayers = [convolution2dLayer(3,64,"Name","res2a_branch2a","Padding",1)
            batchNormalizationLayer("Name","bn2a_branch2a")
            reluLayer("Name","res2a_branch2a_relu")
            convolution2dLayer(3,64,"Name","res2a_branch2b","Padding",1)
            batchNormalizationLayer("Name","bn2a_branch2b")];
        lgraph = addLayers(lgraph,tempLayers);


        tempLayers = [additionLayer(2,"Name","res2a")
            reluLayer("Name","res2a_relu")];
        lgraph = addLayers(lgraph,tempLayers);


        tempLayers = [convolution2dLayer(3,64,"Name","res2b_branch2a","Padding",1)
            batchNormalizationLayer("Name","bn2b_branch2a")
            reluLayer("Name","res2b_branch2a_relu")
            convolution2dLayer(3,64,"Name","res2b_branch2b","Padding",1)
            batchNormalizationLayer("Name","bn2b_branch2b")];
        lgraph = addLayers(lgraph,tempLayers);

        tempLayers = [additionLayer(2,"Name","res2b")
            reluLayer("Name","res2b_relu")];
        lgraph = addLayers(lgraph,tempLayers);

        tempLayers = [convolution2dLayer(1,128,"Name","res3a_branch1","Stride",2)
            batchNormalizationLayer("Name","bn3a_branch1")];
        lgraph = addLayers(lgraph,tempLayers);

        tempLayers = [convolution2dLayer(3,128,"Name","res3a_branch2a","Padding",1,"Stride",2)
            batchNormalizationLayer("Name","bn3a_branch2a")
            reluLayer("Name","res3a_branch2a_relu")
            convolution2dLayer(3,128,"Name","res3a_branch2b","Padding",1)
            batchNormalizationLayer("Name","bn3a_branch2b")];
        lgraph = addLayers(lgraph,tempLayers);

        tempLayers = [additionLayer(2,"Name","res3a")
            reluLayer("Name","res3a_relu")];
        lgraph = addLayers(lgraph,tempLayers);

        tempLayers = [convolution2dLayer(3,128,"Name","res3b_branch2a","Padding",1)
            batchNormalizationLayer("Name","bn3b_branch2a")
            reluLayer("Name","res3b_branch2a_relu")
            convolution2dLayer(3,128,"Name","res3b_branch2b","Padding",1)
            batchNormalizationLayer("Name","bn3b_branch2b")];
        lgraph = addLayers(lgraph,tempLayers);

        tempLayers = [additionLayer(2,"Name","res3b")
            reluLayer("Name","res3b_relu")];
        lgraph = addLayers(lgraph,tempLayers);

        tempLayers = [convolution2dLayer(1,256,"Name","res4a_branch1","Stride",2)
            batchNormalizationLayer("Name","bn4a_branch1")];
        lgraph = addLayers(lgraph,tempLayers);

        tempLayers = [convolution2dLayer(3,256,"Name","res4a_branch2a","Padding",1,"Stride",2)
            batchNormalizationLayer("Name","bn4a_branch2a")
            reluLayer("Name","res4a_branch2a_relu")
            convolution2dLayer(3,256,"Name","res4a_branch2b","Padding",1)
            batchNormalizationLayer("Name","bn4a_branch2b")];
        lgraph = addLayers(lgraph,tempLayers);

        tempLayers = [additionLayer(2,"Name","res4a")
            reluLayer("Name","res4a_relu")];
        lgraph = addLayers(lgraph,tempLayers);

        tempLayers = [convolution2dLayer(3,256,"Name","res4b_branch2a","Padding",1)
            batchNormalizationLayer("Name","bn4b_branch2a")
            reluLayer("Name","res4b_branch2a_relu")
            convolution2dLayer(3,256,"Name","res4b_branch2b","Padding",1)
            batchNormalizationLayer("Name","bn4b_branch2b")];
        lgraph = addLayers(lgraph,tempLayers);

        tempLayers = [additionLayer(2,"Name","res4b")
            reluLayer("Name","res4b_relu")];
        lgraph = addLayers(lgraph,tempLayers);

        tempLayers = [convolution2dLayer(3,512,"Name","res5a_branch2a","Padding",1,"Stride",2)
            batchNormalizationLayer("Name","bn5a_branch2a")
            reluLayer("Name","res5a_branch2a_relu")
            convolution2dLayer(3,512,"Name","res5a_branch2b","Padding",1)
            batchNormalizationLayer("Name","bn5a_branch2b")];
        lgraph = addLayers(lgraph,tempLayers);

        tempLayers = [convolution2dLayer(1,512,"Name","res5a_branch1","Stride",2)
            batchNormalizationLayer("Name","bn5a_branch1")];
        lgraph = addLayers(lgraph,tempLayers);

        tempLayers = [additionLayer(2,"Name","res5a")
            reluLayer("Name","res5a_relu")];
        lgraph = addLayers(lgraph,tempLayers);

        tempLayers = [convolution2dLayer(3,512,"Name","res5b_branch2a","Padding",1)
            batchNormalizationLayer("Name","bn5b_branch2a")
            reluLayer("Name","res5b_branch2a_relu")
            convolution2dLayer(3,512,"Name","res5b_branch2b","Padding",1)
            batchNormalizationLayer("Name","bn5b_branch2b")];
        lgraph = addLayers(lgraph,tempLayers);

        tempLayers = [additionLayer(2,"Name","res5b")
            reluLayer("Name","res5b_relu")
            globalAveragePooling2dLayer("Name","pool5")
            fullyConnectedLayer(1,"Name","fc1")
            regressionLayer("Name","RegressionLayer_predictions")];
        lgraph = addLayers(lgraph,tempLayers);


        lgraph = connectLayers(lgraph,"pool1","res2a_branch2a");
        lgraph = connectLayers(lgraph,"pool1","res2a/in2");
        lgraph = connectLayers(lgraph,"bn2a_branch2b","res2a/in1");
        lgraph = connectLayers(lgraph,"res2a_relu","res2b_branch2a");
        lgraph = connectLayers(lgraph,"res2a_relu","res2b/in2");
        lgraph = connectLayers(lgraph,"bn2b_branch2b","res2b/in1");
        lgraph = connectLayers(lgraph,"res2b_relu","res3a_branch1");
        lgraph = connectLayers(lgraph,"res2b_relu","res3a_branch2a");
        lgraph = connectLayers(lgraph,"bn3a_branch2b","res3a/in1");
        lgraph = connectLayers(lgraph,"bn3a_branch1","res3a/in2");
        lgraph = connectLayers(lgraph,"res3a_relu","res3b_branch2a");
        lgraph = connectLayers(lgraph,"res3a_relu","res3b/in2");
        lgraph = connectLayers(lgraph,"bn3b_branch2b","res3b/in1");
        lgraph = connectLayers(lgraph,"res3b_relu","res4a_branch1");
        lgraph = connectLayers(lgraph,"res3b_relu","res4a_branch2a");
        lgraph = connectLayers(lgraph,"bn4a_branch1","res4a/in2");
        lgraph = connectLayers(lgraph,"bn4a_branch2b","res4a/in1");
        lgraph = connectLayers(lgraph,"res4a_relu","res4b_branch2a");
        lgraph = connectLayers(lgraph,"res4a_relu","res4b/in2");
        lgraph = connectLayers(lgraph,"bn4b_branch2b","res4b/in1");
        lgraph = connectLayers(lgraph,"res4b_relu","res5a_branch2a");
        lgraph = connectLayers(lgraph,"res4b_relu","res5a_branch1");
        lgraph = connectLayers(lgraph,"bn5a_branch1","res5a/in2");
        lgraph = connectLayers(lgraph,"bn5a_branch2b","res5a/in1");
        lgraph = connectLayers(lgraph,"res5a_relu","res5b_branch2a");
        lgraph = connectLayers(lgraph,"res5a_relu","res5b/in2");
        lgraph = connectLayers(lgraph,"bn5b_branch2b","res5b/in1");

        L = lgraph ;
    case 'googlenet'
        lgraph = layerGraph();

        tempLayers = [
            imageInputLayer(Dimension,"Name","data")
            convolution2dLayer([size(Dimension,1) size(Dimension,2)],64,"Name","conv1-7x7_s2","BiasLearnRateFactor",2,"Padding",0,"Stride",[size(Dimension,1) size(Dimension,2)])
            reluLayer("Name","conv1-relu_7x7")
            maxPooling2dLayer(1,"Name","pool1-3x3_s2","Padding",0,"Stride",2)
            crossChannelNormalizationLayer(5,"Name","pool1-norm1","K",1)
            convolution2dLayer(1,64,"Name","conv2-3x3_reduce","BiasLearnRateFactor",2)
            reluLayer("Name","conv2-relu_3x3_reduce")
            convolution2dLayer(3,192,"Name","conv2-3x3","BiasLearnRateFactor",2,"Padding",1)
            reluLayer("Name","conv2-relu_3x3")
            crossChannelNormalizationLayer(5,"Name","conv2-norm2","K",1)
            maxPooling2dLayer([2 2],"Name","pool2-3x3_s2","Padding",[0 1 0 1],"Stride",[2 2])];
        lgraph = addLayers(lgraph,tempLayers);

        tempLayers = [
            maxPooling2dLayer([3 3],"Name","inception_3a-pool","Padding",[1 1 1 1])
            convolution2dLayer([1 1],32,"Name","inception_3a-pool_proj","BiasLearnRateFactor",2)
            reluLayer("Name","inception_3a-relu_pool_proj")];
        lgraph = addLayers(lgraph,tempLayers);

        tempLayers = [
            convolution2dLayer([1 1],96,"Name","inception_3a-3x3_reduce","BiasLearnRateFactor",2)
            reluLayer("Name","inception_3a-relu_3x3_reduce")
            convolution2dLayer([3 3],128,"Name","inception_3a-3x3","BiasLearnRateFactor",2,"Padding",[1 1 1 1])
            reluLayer("Name","inception_3a-relu_3x3")];
        lgraph = addLayers(lgraph,tempLayers);

        tempLayers = [
            convolution2dLayer([1 1],64,"Name","inception_3a-1x1","BiasLearnRateFactor",2)
            reluLayer("Name","inception_3a-relu_1x1")];
        lgraph = addLayers(lgraph,tempLayers);

        tempLayers = [
            convolution2dLayer([1 1],16,"Name","inception_3a-5x5_reduce","BiasLearnRateFactor",2)
            reluLayer("Name","inception_3a-relu_5x5_reduce")
            convolution2dLayer([5 5],32,"Name","inception_3a-5x5","BiasLearnRateFactor",2,"Padding",[2 2 2 2])
            reluLayer("Name","inception_3a-relu_5x5")];
        lgraph = addLayers(lgraph,tempLayers);

        tempLayers = depthConcatenationLayer(4,"Name","inception_3a-output");
        lgraph = addLayers(lgraph,tempLayers);

        tempLayers = [
            convolution2dLayer([1 1],128,"Name","inception_3b-3x3_reduce","BiasLearnRateFactor",2)
            reluLayer("Name","inception_3b-relu_3x3_reduce")
            convolution2dLayer([3 3],192,"Name","inception_3b-3x3","BiasLearnRateFactor",2,"Padding",[1 1 1 1])
            reluLayer("Name","inception_3b-relu_3x3")];
        lgraph = addLayers(lgraph,tempLayers);

        tempLayers = [
            convolution2dLayer([1 1],32,"Name","inception_3b-5x5_reduce","BiasLearnRateFactor",2)
            reluLayer("Name","inception_3b-relu_5x5_reduce")
            convolution2dLayer([5 5],96,"Name","inception_3b-5x5","BiasLearnRateFactor",2,"Padding",[2 2 2 2])
            reluLayer("Name","inception_3b-relu_5x5")];
        lgraph = addLayers(lgraph,tempLayers);

        tempLayers = [
            convolution2dLayer([1 1],128,"Name","inception_3b-1x1","BiasLearnRateFactor",2)
            reluLayer("Name","inception_3b-relu_1x1")];
        lgraph = addLayers(lgraph,tempLayers);

        tempLayers = [
            maxPooling2dLayer([3 3],"Name","inception_3b-pool","Padding",[1 1 1 1])
            convolution2dLayer([1 1],64,"Name","inception_3b-pool_proj","BiasLearnRateFactor",2)
            reluLayer("Name","inception_3b-relu_pool_proj")];
        lgraph = addLayers(lgraph,tempLayers);

        tempLayers = [
            depthConcatenationLayer(4,"Name","inception_3b-output")
            maxPooling2dLayer([2 2],"Name","pool3-3x3_s2","Padding",[0 1 0 1],"Stride",[2 2])];
        lgraph = addLayers(lgraph,tempLayers);

        tempLayers = [
            convolution2dLayer([1 1],16,"Name","inception_4a-5x5_reduce","BiasLearnRateFactor",2)
            reluLayer("Name","inception_4a-relu_5x5_reduce")
            convolution2dLayer([5 5],48,"Name","inception_4a-5x5","BiasLearnRateFactor",2,"Padding",[2 2 2 2])
            reluLayer("Name","inception_4a-relu_5x5")];
        lgraph = addLayers(lgraph,tempLayers);

        tempLayers = [
            convolution2dLayer([1 1],192,"Name","inception_4a-1x1","BiasLearnRateFactor",2)
            reluLayer("Name","inception_4a-relu_1x1")];
        lgraph = addLayers(lgraph,tempLayers);

        tempLayers = [
            convolution2dLayer([1 1],96,"Name","inception_4a-3x3_reduce","BiasLearnRateFactor",2)
            reluLayer("Name","inception_4a-relu_3x3_reduce")
            convolution2dLayer([3 3],208,"Name","inception_4a-3x3","BiasLearnRateFactor",2,"Padding",[1 1 1 1])
            reluLayer("Name","inception_4a-relu_3x3")];
        lgraph = addLayers(lgraph,tempLayers);

        tempLayers = [
            maxPooling2dLayer([3 3],"Name","inception_4a-pool","Padding",[1 1 1 1])
            convolution2dLayer([1 1],64,"Name","inception_4a-pool_proj","BiasLearnRateFactor",2)
            reluLayer("Name","inception_4a-relu_pool_proj")];
        lgraph = addLayers(lgraph,tempLayers);

        tempLayers = depthConcatenationLayer(4,"Name","inception_4a-output");
        lgraph = addLayers(lgraph,tempLayers);

        tempLayers = [
            convolution2dLayer([1 1],24,"Name","inception_4b-5x5_reduce","BiasLearnRateFactor",2)
            reluLayer("Name","inception_4b-relu_5x5_reduce")
            convolution2dLayer([5 5],64,"Name","inception_4b-5x5","BiasLearnRateFactor",2,"Padding",[2 2 2 2])
            reluLayer("Name","inception_4b-relu_5x5")];
        lgraph = addLayers(lgraph,tempLayers);

        tempLayers = [
            convolution2dLayer([1 1],112,"Name","inception_4b-3x3_reduce","BiasLearnRateFactor",2)
            reluLayer("Name","inception_4b-relu_3x3_reduce")
            convolution2dLayer([3 3],224,"Name","inception_4b-3x3","BiasLearnRateFactor",2,"Padding",[1 1 1 1])
            reluLayer("Name","inception_4b-relu_3x3")];
        lgraph = addLayers(lgraph,tempLayers);

        tempLayers = [
            maxPooling2dLayer([3 3],"Name","inception_4b-pool","Padding",[1 1 1 1])
            convolution2dLayer([1 1],64,"Name","inception_4b-pool_proj","BiasLearnRateFactor",2)
            reluLayer("Name","inception_4b-relu_pool_proj")];
        lgraph = addLayers(lgraph,tempLayers);

        tempLayers = [
            convolution2dLayer([1 1],160,"Name","inception_4b-1x1","BiasLearnRateFactor",2)
            reluLayer("Name","inception_4b-relu_1x1")];
        lgraph = addLayers(lgraph,tempLayers);

        tempLayers = depthConcatenationLayer(4,"Name","inception_4b-output");
        lgraph = addLayers(lgraph,tempLayers);

        tempLayers = [
            maxPooling2dLayer([3 3],"Name","inception_4c-pool","Padding",[1 1 1 1])
            convolution2dLayer([1 1],64,"Name","inception_4c-pool_proj","BiasLearnRateFactor",2)
            reluLayer("Name","inception_4c-relu_pool_proj")];
        lgraph = addLayers(lgraph,tempLayers);

        tempLayers = [
            convolution2dLayer([1 1],128,"Name","inception_4c-1x1","BiasLearnRateFactor",2)
            reluLayer("Name","inception_4c-relu_1x1")];
        lgraph = addLayers(lgraph,tempLayers);

        tempLayers = [
            convolution2dLayer([1 1],128,"Name","inception_4c-3x3_reduce","BiasLearnRateFactor",2)
            reluLayer("Name","inception_4c-relu_3x3_reduce")
            convolution2dLayer([3 3],256,"Name","inception_4c-3x3","BiasLearnRateFactor",2,"Padding",[1 1 1 1])
            reluLayer("Name","inception_4c-relu_3x3")];
        lgraph = addLayers(lgraph,tempLayers);

        tempLayers = [
            convolution2dLayer([1 1],24,"Name","inception_4c-5x5_reduce","BiasLearnRateFactor",2)
            reluLayer("Name","inception_4c-relu_5x5_reduce")
            convolution2dLayer([5 5],64,"Name","inception_4c-5x5","BiasLearnRateFactor",2,"Padding",[2 2 2 2])
            reluLayer("Name","inception_4c-relu_5x5")];
        lgraph = addLayers(lgraph,tempLayers);

        tempLayers = depthConcatenationLayer(4,"Name","inception_4c-output");
        lgraph = addLayers(lgraph,tempLayers);

        tempLayers = [
            maxPooling2dLayer([3 3],"Name","inception_4d-pool","Padding",[1 1 1 1])
            convolution2dLayer([1 1],64,"Name","inception_4d-pool_proj","BiasLearnRateFactor",2)
            reluLayer("Name","inception_4d-relu_pool_proj")];
        lgraph = addLayers(lgraph,tempLayers);

        tempLayers = [
            convolution2dLayer([1 1],144,"Name","inception_4d-3x3_reduce","BiasLearnRateFactor",2)
            reluLayer("Name","inception_4d-relu_3x3_reduce")
            convolution2dLayer([3 3],288,"Name","inception_4d-3x3","BiasLearnRateFactor",2,"Padding",[1 1 1 1])
            reluLayer("Name","inception_4d-relu_3x3")];
        lgraph = addLayers(lgraph,tempLayers);

        tempLayers = [
            convolution2dLayer([1 1],32,"Name","inception_4d-5x5_reduce","BiasLearnRateFactor",2)
            reluLayer("Name","inception_4d-relu_5x5_reduce")
            convolution2dLayer([5 5],64,"Name","inception_4d-5x5","BiasLearnRateFactor",2,"Padding",[2 2 2 2])
            reluLayer("Name","inception_4d-relu_5x5")];
        lgraph = addLayers(lgraph,tempLayers);

        tempLayers = [
            convolution2dLayer([1 1],112,"Name","inception_4d-1x1","BiasLearnRateFactor",2)
            reluLayer("Name","inception_4d-relu_1x1")];
        lgraph = addLayers(lgraph,tempLayers);

        tempLayers = depthConcatenationLayer(4,"Name","inception_4d-output");
        lgraph = addLayers(lgraph,tempLayers);

        tempLayers = [
            convolution2dLayer([1 1],256,"Name","inception_4e-1x1","BiasLearnRateFactor",2)
            reluLayer("Name","inception_4e-relu_1x1")];
        lgraph = addLayers(lgraph,tempLayers);

        tempLayers = [
            convolution2dLayer([1 1],160,"Name","inception_4e-3x3_reduce","BiasLearnRateFactor",2)
            reluLayer("Name","inception_4e-relu_3x3_reduce")
            convolution2dLayer([3 3],320,"Name","inception_4e-3x3","BiasLearnRateFactor",2,"Padding",[1 1 1 1])
            reluLayer("Name","inception_4e-relu_3x3")];
        lgraph = addLayers(lgraph,tempLayers);

        tempLayers = [
            maxPooling2dLayer([3 3],"Name","inception_4e-pool","Padding",[1 1 1 1])
            convolution2dLayer([1 1],128,"Name","inception_4e-pool_proj","BiasLearnRateFactor",2)
            reluLayer("Name","inception_4e-relu_pool_proj")];
        lgraph = addLayers(lgraph,tempLayers);

        tempLayers = [
            convolution2dLayer([1 1],32,"Name","inception_4e-5x5_reduce","BiasLearnRateFactor",2)
            reluLayer("Name","inception_4e-relu_5x5_reduce")
            convolution2dLayer([5 5],128,"Name","inception_4e-5x5","BiasLearnRateFactor",2,"Padding",[2 2 2 2])
            reluLayer("Name","inception_4e-relu_5x5")];
        lgraph = addLayers(lgraph,tempLayers);

        tempLayers = [
            depthConcatenationLayer(4,"Name","inception_4e-output")
            maxPooling2dLayer([2 2],"Name","pool4-3x3_s2","Padding",[0 1 0 1],"Stride",[2 2])];
        lgraph = addLayers(lgraph,tempLayers);

        tempLayers = [
            convolution2dLayer([1 1],160,"Name","inception_5a-3x3_reduce","BiasLearnRateFactor",2)
            reluLayer("Name","inception_5a-relu_3x3_reduce")
            convolution2dLayer([3 3],320,"Name","inception_5a-3x3","BiasLearnRateFactor",2,"Padding",[1 1 1 1])
            reluLayer("Name","inception_5a-relu_3x3")];
        lgraph = addLayers(lgraph,tempLayers);

        tempLayers = [
            maxPooling2dLayer([3 3],"Name","inception_5a-pool","Padding",[1 1 1 1])
            convolution2dLayer([1 1],128,"Name","inception_5a-pool_proj","BiasLearnRateFactor",2)
            reluLayer("Name","inception_5a-relu_pool_proj")];
        lgraph = addLayers(lgraph,tempLayers);

        tempLayers = [
            convolution2dLayer([1 1],256,"Name","inception_5a-1x1","BiasLearnRateFactor",2)
            reluLayer("Name","inception_5a-relu_1x1")];
        lgraph = addLayers(lgraph,tempLayers);

        tempLayers = [
            convolution2dLayer([1 1],32,"Name","inception_5a-5x5_reduce","BiasLearnRateFactor",2)
            reluLayer("Name","inception_5a-relu_5x5_reduce")
            convolution2dLayer([5 5],128,"Name","inception_5a-5x5","BiasLearnRateFactor",2,"Padding",[2 2 2 2])
            reluLayer("Name","inception_5a-relu_5x5")];
        lgraph = addLayers(lgraph,tempLayers);

        tempLayers = depthConcatenationLayer(4,"Name","inception_5a-output");
        lgraph = addLayers(lgraph,tempLayers);

        tempLayers = [
            convolution2dLayer([1 1],384,"Name","inception_5b-1x1","BiasLearnRateFactor",2)
            reluLayer("Name","inception_5b-relu_1x1")];
        lgraph = addLayers(lgraph,tempLayers);

        tempLayers = [
            maxPooling2dLayer([3 3],"Name","inception_5b-pool","Padding",[1 1 1 1])
            convolution2dLayer([1 1],128,"Name","inception_5b-pool_proj","BiasLearnRateFactor",2)
            reluLayer("Name","inception_5b-relu_pool_proj")];
        lgraph = addLayers(lgraph,tempLayers);

        tempLayers = [
            convolution2dLayer([1 1],192,"Name","inception_5b-3x3_reduce","BiasLearnRateFactor",2)
            reluLayer("Name","inception_5b-relu_3x3_reduce")
            convolution2dLayer([3 3],384,"Name","inception_5b-3x3","BiasLearnRateFactor",2,"Padding",[1 1 1 1])
            reluLayer("Name","inception_5b-relu_3x3")];
        lgraph = addLayers(lgraph,tempLayers);

        tempLayers = [
            convolution2dLayer([1 1],48,"Name","inception_5b-5x5_reduce","BiasLearnRateFactor",2)
            reluLayer("Name","inception_5b-relu_5x5_reduce")
            convolution2dLayer([5 5],128,"Name","inception_5b-5x5","BiasLearnRateFactor",2,"Padding",[2 2 2 2])
            reluLayer("Name","inception_5b-relu_5x5")];
        lgraph = addLayers(lgraph,tempLayers);

        tempLayers = [
            depthConcatenationLayer(4,"Name","inception_5b-output")
            globalAveragePooling2dLayer("Name","pool5-7x7_s1")
            ...dropoutLayer(0.4,"Name","pool5-drop_7x7_s1")
            fullyConnectedLayer(1,"Name","loss3-classifier","BiasLearnRateFactor",2)
            ...softmaxLayer("Name","prob")
            ...classificationLayer("Name","output")
            regressionLayer
            ];
        lgraph = addLayers(lgraph,tempLayers);


        lgraph = connectLayers(lgraph,"pool2-3x3_s2","inception_3a-pool");
        lgraph = connectLayers(lgraph,"pool2-3x3_s2","inception_3a-3x3_reduce");
        lgraph = connectLayers(lgraph,"pool2-3x3_s2","inception_3a-1x1");
        lgraph = connectLayers(lgraph,"pool2-3x3_s2","inception_3a-5x5_reduce");
        lgraph = connectLayers(lgraph,"inception_3a-relu_1x1","inception_3a-output/in1");
        lgraph = connectLayers(lgraph,"inception_3a-relu_pool_proj","inception_3a-output/in4");
        lgraph = connectLayers(lgraph,"inception_3a-relu_3x3","inception_3a-output/in2");
        lgraph = connectLayers(lgraph,"inception_3a-relu_5x5","inception_3a-output/in3");
        lgraph = connectLayers(lgraph,"inception_3a-output","inception_3b-3x3_reduce");
        lgraph = connectLayers(lgraph,"inception_3a-output","inception_3b-5x5_reduce");
        lgraph = connectLayers(lgraph,"inception_3a-output","inception_3b-1x1");
        lgraph = connectLayers(lgraph,"inception_3a-output","inception_3b-pool");
        lgraph = connectLayers(lgraph,"inception_3b-relu_3x3","inception_3b-output/in2");
        lgraph = connectLayers(lgraph,"inception_3b-relu_1x1","inception_3b-output/in1");
        lgraph = connectLayers(lgraph,"inception_3b-relu_pool_proj","inception_3b-output/in4");
        lgraph = connectLayers(lgraph,"inception_3b-relu_5x5","inception_3b-output/in3");
        lgraph = connectLayers(lgraph,"pool3-3x3_s2","inception_4a-5x5_reduce");
        lgraph = connectLayers(lgraph,"pool3-3x3_s2","inception_4a-1x1");
        lgraph = connectLayers(lgraph,"pool3-3x3_s2","inception_4a-3x3_reduce");
        lgraph = connectLayers(lgraph,"pool3-3x3_s2","inception_4a-pool");
        lgraph = connectLayers(lgraph,"inception_4a-relu_1x1","inception_4a-output/in1");
        lgraph = connectLayers(lgraph,"inception_4a-relu_5x5","inception_4a-output/in3");
        lgraph = connectLayers(lgraph,"inception_4a-relu_3x3","inception_4a-output/in2");
        lgraph = connectLayers(lgraph,"inception_4a-relu_pool_proj","inception_4a-output/in4");
        lgraph = connectLayers(lgraph,"inception_4a-output","inception_4b-5x5_reduce");
        lgraph = connectLayers(lgraph,"inception_4a-output","inception_4b-3x3_reduce");
        lgraph = connectLayers(lgraph,"inception_4a-output","inception_4b-pool");
        lgraph = connectLayers(lgraph,"inception_4a-output","inception_4b-1x1");
        lgraph = connectLayers(lgraph,"inception_4b-relu_5x5","inception_4b-output/in3");
        lgraph = connectLayers(lgraph,"inception_4b-relu_pool_proj","inception_4b-output/in4");
        lgraph = connectLayers(lgraph,"inception_4b-relu_1x1","inception_4b-output/in1");
        lgraph = connectLayers(lgraph,"inception_4b-relu_3x3","inception_4b-output/in2");
        lgraph = connectLayers(lgraph,"inception_4b-output","inception_4c-pool");
        lgraph = connectLayers(lgraph,"inception_4b-output","inception_4c-1x1");
        lgraph = connectLayers(lgraph,"inception_4b-output","inception_4c-3x3_reduce");
        lgraph = connectLayers(lgraph,"inception_4b-output","inception_4c-5x5_reduce");
        lgraph = connectLayers(lgraph,"inception_4c-relu_1x1","inception_4c-output/in1");
        lgraph = connectLayers(lgraph,"inception_4c-relu_5x5","inception_4c-output/in3");
        lgraph = connectLayers(lgraph,"inception_4c-relu_pool_proj","inception_4c-output/in4");
        lgraph = connectLayers(lgraph,"inception_4c-relu_3x3","inception_4c-output/in2");
        lgraph = connectLayers(lgraph,"inception_4c-output","inception_4d-pool");
        lgraph = connectLayers(lgraph,"inception_4c-output","inception_4d-3x3_reduce");
        lgraph = connectLayers(lgraph,"inception_4c-output","inception_4d-5x5_reduce");
        lgraph = connectLayers(lgraph,"inception_4c-output","inception_4d-1x1");
        lgraph = connectLayers(lgraph,"inception_4d-relu_5x5","inception_4d-output/in3");
        lgraph = connectLayers(lgraph,"inception_4d-relu_pool_proj","inception_4d-output/in4");
        lgraph = connectLayers(lgraph,"inception_4d-relu_3x3","inception_4d-output/in2");
        lgraph = connectLayers(lgraph,"inception_4d-relu_1x1","inception_4d-output/in1");
        lgraph = connectLayers(lgraph,"inception_4d-output","inception_4e-1x1");
        lgraph = connectLayers(lgraph,"inception_4d-output","inception_4e-3x3_reduce");
        lgraph = connectLayers(lgraph,"inception_4d-output","inception_4e-pool");
        lgraph = connectLayers(lgraph,"inception_4d-output","inception_4e-5x5_reduce");
        lgraph = connectLayers(lgraph,"inception_4e-relu_pool_proj","inception_4e-output/in4");
        lgraph = connectLayers(lgraph,"inception_4e-relu_3x3","inception_4e-output/in2");
        lgraph = connectLayers(lgraph,"inception_4e-relu_1x1","inception_4e-output/in1");
        lgraph = connectLayers(lgraph,"inception_4e-relu_5x5","inception_4e-output/in3");
        lgraph = connectLayers(lgraph,"pool4-3x3_s2","inception_5a-3x3_reduce");
        lgraph = connectLayers(lgraph,"pool4-3x3_s2","inception_5a-pool");
        lgraph = connectLayers(lgraph,"pool4-3x3_s2","inception_5a-1x1");
        lgraph = connectLayers(lgraph,"pool4-3x3_s2","inception_5a-5x5_reduce");
        lgraph = connectLayers(lgraph,"inception_5a-relu_1x1","inception_5a-output/in1");
        lgraph = connectLayers(lgraph,"inception_5a-relu_pool_proj","inception_5a-output/in4");
        lgraph = connectLayers(lgraph,"inception_5a-relu_3x3","inception_5a-output/in2");
        lgraph = connectLayers(lgraph,"inception_5a-relu_5x5","inception_5a-output/in3");
        lgraph = connectLayers(lgraph,"inception_5a-output","inception_5b-1x1");
        lgraph = connectLayers(lgraph,"inception_5a-output","inception_5b-pool");
        lgraph = connectLayers(lgraph,"inception_5a-output","inception_5b-3x3_reduce");
        lgraph = connectLayers(lgraph,"inception_5a-output","inception_5b-5x5_reduce");
        lgraph = connectLayers(lgraph,"inception_5b-relu_3x3","inception_5b-output/in2");
        lgraph = connectLayers(lgraph,"inception_5b-relu_pool_proj","inception_5b-output/in4");
        lgraph = connectLayers(lgraph,"inception_5b-relu_5x5","inception_5b-output/in3");
        lgraph = connectLayers(lgraph,"inception_5b-relu_1x1","inception_5b-output/in1");

        L = lgraph ;
end
end

